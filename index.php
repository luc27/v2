<?php
    include_once("default/include.php");

    $oPageConfig = new clsPageConfig();

    $bAjax = isset($_POST['bAjax']);

    $aPaths = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    $aAllowedPages = array("account", "content", "helper", "multiplayer", "game");

    require 'Exception.php';
    require 'PHPMailer.php';
    require 'SMTP.php';

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    if (is_array($aPaths) && false) {

        if (!in_array($aPaths[1], array("api"))) {

             $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                //Server settings
                $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'lvps92-51-164-213.dedicated.hosteurope.de';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'vikings@lvps92-51-164-213.dedicated.hosteurope.de';                 // SMTP username
                $mail->Password = 'yp9X5i_5';                           // SMTP password
                $mail->SMTPSecure = false;                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                //Recipients
                $mail->setFrom('vikings@lvps92-51-164-213.dedicated.hosteurope.de', 'Mailer');
                $mail->addAddress('ennemoserandre@yahoo.de', 'Joe User');     // Add a recipient
                /*$mail->addAddress('ellen@example.com');               // Name is optional
                $mail->addReplyTo('info@example.com', 'Information');
                $mail->addCC('cc@example.com');
                $mail->addBCC('bcc@example.com');*/

                //Attachments
            /*    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name*/

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Here is the subject';
                $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                echo 'Message has been sent';
            } catch (Exception $e) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            }

            die();
        }
    }
    
    if (is_array($aPaths)) {
        if (in_array($aPaths[1], array("api"))) {
            if (in_array($aPaths[2], $aAllowedPages)) {
                if(empty($aPaths[3]) || !file_exists($aPaths[1] . "/" .  $aPaths[2] . "/" . $aPaths[3] . ".php")) {
                    include_once("api/helper/Info.php");
                } else {
                    include_once($aPaths[1] . "/" .  $aPaths[2] . "/" . $aPaths[3] . ".php");
                }
            } else {
                include_once("api/helper/Info.php");
            }
        } else {
            if (!$bAjax) {
                include_once("pages/header.php");
            }
            if(empty($aPaths[1]) || empty($aPaths[2]) || !file_exists("pages/" . $aPaths[1] . "/" . $aPaths[2] . ".php")) {
                // include_once("pages/home.php");
                include_once("pages/game/home.php");
            } else {
                include_once("pages/" . $aPaths[1] . "/" . $aPaths[2] . ".php");
            }
            if (!$bAjax) {
                include_once("pages/footer.php");
            }
        }
    }
?>
