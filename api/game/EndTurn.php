<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $lMatchID = $_GET['m'];
    $lAccountID = $_SESSION['lAccountID'];
    
    if (!isset($lAccountID))
    $lAccountID = $_GET['a'];
    
    if (!isset($lMatchID))
    die();
    
    $oMatch = new clsMatch();
    $oMatch->sQueryType = "byID";
    $oMatch->lMatchID = $lMatchID;
    $oMatch->Load();
    
    $bAttacker = $oMatch->GetAccountID() == $lAccountID;
    $bDefendand = $oMatch->GetAccountID2() == $lAccountID;
    if (!$bAttacker && !$bDefendand)
    die();
    
    if (!$oMatch->IsOwnTurn())
    die();
    
    $oMatch->EndTurn();
    
    $oAccount = new clsAccount();
    $oAccount->lAccountID = $oMatch->GetCurrentAccountID();
    $oAccount->Load();
    
    if ($oAccount->IsAI()) {
        $oPageConfig->To("/api/game/EndTurn?m=$lMatchID&a=$oMatch->GetCurrentAccountID()");
    } else {
        $oPageConfig->To("/game/match?m=$lMatchID");
    }
 ?>
