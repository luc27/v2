<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    header('Content-Type: application/json; charset=utf-8');
    
    $lAccountID = $_SESSION['lAccountID'];
    
    if (isset($_POST['MatchID'])) {
        $lMatchID = $_POST['MatchID'];
    } else {
        $lMatchID = "";
    }
    
    if (isset($_POST['Units'])) {
        $sUnits = $_POST['Units'];
    } else {
        $sUnits = "";
    }
    
    $bError = false;
    
    if (!empty($lAccountID) && !empty($sUnits) && !empty($lMatchID)) {
    
        $oUnit = new clsUnit();
        $oUnit->sQueryType = "byunitid";
        
        foreach (explode(",", $sUnits) as $Unit) {

            $oUnit->lUnitID = $Unit;
            $oUnit->Load(); 
            
            if ($oUnit->Eof()) {
                $bError = true;
            }
            
            if ($oUnit->GetUnitStatus() == 0) {
                $sSql = "UPDATE ProgressUnits SET MatchID = $lMatchID, CurrentAP = MaxAP WHERE `ID` = $Unit";
                $oResult = DB::$oConn->query($sSql);
            } else {
                $bError = true;
            }
        
        }
        if ($bError) {
            $aJsonResult = array(
                'Status' => 0,
                'Error' => clsDictionary::GetDicItem('Web_MatchMakeParamsReq')
            );
        } else {
            $sSql = "UPDATE `Match` SET Status = (Status + 1) WHERE `ID` = $lMatchID";
            $oResult = DB::$oConn->query($sSql);
            
            
            
            // AI
            $oMatch = new clsMatch();
            $oMatch->sQueryType = "byID";
            $oMatch->lMatchID = $lMatchID;
            $oMatch->Load();
            $oAccount = new clsAccount();
            $oAccount->lAccountID = $oMatch->GetAccountID2();
            $oAccount->Load();
            if ($oAccount->IsAI()) {
            
                $oProfile = new clsProfile();
                $oProfile->sQueryType = "byAccountID";
                $oProfile->lAccountID = $oAccount->lAccountID;
                $oProfile->Load();
                
                // create AI Unit
                $newRecruit = DB::$oConn->query("CALL AddUnit(" . $oProfile->GetProfileID() . ", 'Recruit')");
                $result = mysqli_fetch_assoc($newRecruit);
                $NewAiUnitID = $result['ID'];
                // assign AI Unit to match
                $oResult = DB::$oConn->query("UPDATE `ProgressUnits` SET `AdHoc` = 1, `MatchID` = '$lMatchID' WHERE `ID` = '$NewAiUnitID'");
                // set match status "ongoing"
                $sSql = "UPDATE `Match` SET Status = 2 WHERE `ID` = $lMatchID";
                $oResult = DB::$oConn->query($sSql);
            }
            
            
            
            $aJsonResult = array(
                'NewAiUnitID' => $NewAiUnitID,
                'Status' => 1,
                'Success' => clsDictionary::GetDicItem('Web_MatchCreated')
            );
        }

    } else {
        $aJsonResult = array(
            'Status' => 0,
            'Error' => clsDictionary::GetDicItem('Web_MatchMakeParamsReq')
        );
    }
    
    echo json_encode($aJsonResult);
 ?>
