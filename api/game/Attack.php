<?php
    header("Content-Type: text/html");

    // check if account unit match
    
    if (isset($_POST['sAttackKey'])) {
        $sAttackKey = $_POST['sAttackKey'];
    } else {
        $sAttackKey = "";
    }
    if (isset($_POST['lUnitIDFrom'])) {
        $lUnitIDFrom = $_POST['lUnitIDFrom'];
    } else {
        $lUnitIDFrom = "";
    }
    if (isset($_POST['lUnitIDTo'])) {
        $lUnitIDTo = $_POST['lUnitIDTo'];
    } else {
        $lUnitIDTo = "";
    }
    //OK
    $oUnitFrom = new clsUnit();
    $oUnitFrom->sQueryType = "byUnitID";
    $oUnitFrom->lUnitID = $lUnitIDFrom;
    $oUnitFrom->Load();
    
    $oSpell = new clsSpell();
    $oSpell->sQueryType = "byKey";
    $oSpell->sSpellKey = $sAttackKey;
    $oSpell->Load();
    
    if ($oUnitFrom->GetUnitAP() < $oSpell->GetApCost()) {
    
        $aJsonResult = array(
            'Status' => 0,
            'Error' => clsDictionary::GetDicItem('Game_OutOfMana')
        );
    
    } else {
     
        $oUnitFrom->ApCost($oSpell->GetApCost());
        
        $oUnitTo = new clsUnit();
        $oUnitTo->sQueryType = "byUnitID";
        $oUnitTo->lUnitID = $lUnitIDTo;
        $oUnitTo->Load();
        
        switch (strtolower($sAttackKey)) {
            case "dot":
                $oUnitTo->AddAura("Dot1", 3, $oUnitFrom->GetUnitID());
                break;

            case "flee":
                $oUnitFrom->Flee();
                break;

            case "melee":
                $oUnitTo->DoDmg($oUnitFrom->GetUnitPhysAttackPower());
                break;
        }
        
        $oUnitTo->Load();
        
        if ($oUnitTo->GetCurrentHP() <= 0) {
            
            $sSql = "UPDATE ProgressUnits SET MatchID = null WHERE `ID` = $lUnitIDTo";
            $oResult = DB::$oConn->query($sSql);
            
            $oMatch = new clsMatch();
            $oMatch->sQueryType = "byID";
            $oMatch->lMatchID = $oUnitTo->GetMatchID();
            $oMatch->Load();
            
            if ($oMatch->GetCurrentTeam() == 1) {
                $sPot = "Pot1";
            } else {
                $sPot = "Pot2";
            }
            
            $sSql = "UPDATE Match SET $sPot = $sPot + " . $oUnitTo->GetUnitGearScore() . " WHERE `MatchID` = " . $oUnitTo->GetMatchID();
            $oResult = DB::$oConn->query($sSql);
        }
        
        $aJsonResult = array(
            'Status' => 1
        );
    
    }
        
    echo json_encode($aJsonResult);
?>
