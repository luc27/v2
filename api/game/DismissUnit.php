<?php
    header("Content-Type: text/html");

    $lUnitID = $_POST['lUnitID'];
    
    $oUnit = new clsUnit();
    $oUnit->sQueryType = "byUnitID";
    $oUnit->lUnitID = $lUnitID;
    $oUnit->Load();
    
    if (!$oUnit->Eof()) {

        if ($oUnit->IsOwn()) {

            $oUnit->Dismiss();
        
        }

    }
?>
