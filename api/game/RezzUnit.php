<?php
    header("Content-Type: text/html");

    $lProfileID = $_POST['lProfileID'];
    $lUnitID = $_POST['lUnitID'];
    $lProfileGold = 0;

    $oProfile = new clsProfile();
    $oProfile->sQueryType = "byProfileID";
    $oProfile->lProfileID = $lProfileID;
    $oProfile->Load();

    if (!$oProfile->Eof()) {

        $lProfileGold = $oProfile->GetProfileGold();

    }

    $oUnit = new clsUnit();
    $oUnit->sQueryType = "byUnitID";
    $oUnit->lUnitID = $lUnitID;
    $oUnit->Load();

    if (!$oUnit->Eof()) {

        $lRezzCosts = $oUnit->GetRezzCosts();

        if ($lRezzCosts <= $lProfileGold) {

            $oProfile->SetProfileGold($lProfileGold - $lRezzCosts);
            $oUnit->Heal();
            // $oUnit->SetUnitStatus(1);

            echo "Success!";
        
        } else {
        
            echo "Not enough gold!";

        }

    }
    
    echo "Error!";

?>
