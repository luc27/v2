<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $lAccountID = $_SESSION['lAccountID'];
    $lMatchID = $_GET['m'];
    
    $oMatch = new clsMatch();
    $oMatch->sQueryType = "byID";
    $oMatch->lMatchID = $lMatchID;
    $oMatch->Load();
    
    if (!$oMatch->IsOver())
    $oPageConfig->To("/game/match?m=$lMatchID");
    
    if (!$oMatch->IsOwnTurn())
    $oPageConfig->To("/game/match?m=$lMatchID");
    
    // gain gold + xp + items
    
    $oMatch->Delete();
    
    $oPageConfig->To("/game/matches");
 ?>
