<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    header('Content-Type: application/json; charset=utf-8');
    
    $lAccountID = $_SESSION['lAccountID'];
    
    if (isset($_POST['lAccountIDPlayer2'])) {
        $lAccountIDPlayer2 = $_POST['lAccountIDPlayer2'];
    } else {
        $lAccountIDPlayer2 = "";
    }
    
    if (isset($_POST['MapKey'])) {
        $sMapKey = $_POST['MapKey'];
    } else {
        $sMapKey = "";
    }
    
    if (empty($lAccountIDPlayer2) && !empty($sMapKey)) {
        
        $oMap = new clsMap();
        $oMap->sQueryType = "byKey";
        $oMap->sMapKey = $sMapKey;
        $oMap->Load();
        $lAccountIDPlayer2 = $oMap->GetAccountID();
    }
    
    if (!empty($lAccountID) && !empty($lAccountIDPlayer2)) {

        
        $sSql = "DELETE FROM `Match` WHERE AccountID = $lAccountID and MapKey = '$sMapKey'";
        $oResult = DB::$oConn->query($sSql);
        
        $sMapSql = "Insert `Match` (AccountID, Player2, MapKey) VALUES ($lAccountID, $lAccountIDPlayer2, '$sMapKey')";
        
        $oResult = DB::$oConn->query($sMapSql);
        
        $lNewMatchID = DB::$oConn->insert_id;
        
        $aJsonResult = array(
            'Status' => 1,
            'Success' => clsDictionary::GetDicItem('Web_MatchCreated'),
            'Created' => $lNewMatchID
        );

    } else {
        $aJsonResult = array(
            'Status' => 0,
            'Error' => clsDictionary::GetDicItem('Web_MatchMakeParamsReq')
        );
    }
    
    echo json_encode($aJsonResult);
 ?>
