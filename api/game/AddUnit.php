<?php
    header("Content-Type: text/html");
    
    $oPageConfig->SecureThisPage('/account/login');
    
    if (isset($_POST['UnitKey'])) {
        $UnitKey = $_POST['UnitKey'];
    } else {
        $UnitKey = "Recruit";
    }
    
    $oProfile = new clsProfile();
    $oProfile->sQueryType = "byProfileID";
    $oProfile->lProfileID = $_SESSION['lProfileID'];
    $oProfile->Load();
    
    if ($oProfile->GetUnits() >= $oProfile->GetMaxUnits())
    $oPageConfig->To("/game/crew");
    
    $oResult = DB::$oConn->query("CALL AddUnit($oProfile->lProfileID, \"$UnitKey\")");
    
    $oPageConfig->To("/game/crew");
?>
