<?php
    $aRows = array();
    $bNameFound = false;

    // Required Params:
    $sTableName = @$_GET['sTable'];
    $lLanguageID = isset($_GET['lLanguageID']) ? $_GET['lLanguageID'] : 'EN';

    if (!empty($sTableName)) {

        header('Content-Type: application/json; charset=utf-8');

        $sSql = "SELECT * FROM $sTableName";

        $aResult = mysqli_query(DB::$oConn, $sSql);

        if($aResult) {

            while($sRow = mysqli_fetch_assoc($aResult))
            {

                $sNameSql = "SELECT DISTINCT " . $lLanguageID . "Text , " . $lLanguageID . "Description FROM Dictionary INNER JOIN $sTableName ON Dictionary.Key = '" . $sTableName . "_$sRow[Key]' LIMIT 1";

                $aNameResult = mysqli_query(DB::$oConn, $sNameSql);

                if($aNameResult) {
                    while($sNameRow = mysqli_fetch_assoc($aNameResult)) {
                        $bNameFound = true;
                    }
                }
                if (!$bNameFound) {
                    $aRows[] = $sRow;
                }
                $bNameFound = false;
            }
        }

        array_walk_recursive($aRows, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        echo json_encode($aRows);

    } else {
        header('Content-Type: text/html; charset=utf-8');
        echo "Table Name required!";
    }
?>
