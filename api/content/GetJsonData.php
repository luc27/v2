<?php
    $aRows = array();
    $lRowCount = 0;
    $aAllowedDictionaryTables = array("Map", "Item", "Unit", "Aura", "Building", "Quest", "Spell");

    // Required Params:
    $sTableName = @$_GET['sTable'];
    $lLanguageID = isset($_GET['lLanguageID']) ? $_GET['lLanguageID'] : 'EN';

    // Optional Params:
    $sState = isset($_GET['sState']) ? $_GET['sState'] : 2;
    $lLimit = isset($_GET['lLimit']) ? $_GET['lLimit'] : 9999999999999;

    if (!empty($sTableName)) {

        header('Content-Type: application/json; charset=utf-8');

        $result = mysqli_query(DB::$oConn, "SHOW COLUMNS FROM $sTableName LIKE 'OrderNumber'");

        if($result->num_rows > 0) {
           $sSql = "SELECT * FROM $sTableName ORDER BY OrderNumber ASC";
       } else {
           $sSql = "SELECT * FROM $sTableName";
       }

        $aResult = mysqli_query(DB::$oConn, $sSql);

        if($aResult) {

            while($sRow = mysqli_fetch_assoc($aResult))
            {

                if ($lRowCount >= $lLimit) {
                    break;
                }

                $sNameSql = "SELECT DISTINCT " . $lLanguageID . "Text , " . $lLanguageID . "Description FROM Dictionary INNER JOIN $sTableName ON Dictionary.Key = '" . $sTableName . "_$sRow[Key]' LIMIT 1";

                $aNameResult = mysqli_query(DB::$oConn, $sNameSql);

                if (is_object( $aNameResult)) {

                    if($aNameResult->num_rows > 0) {
                        while($sNameRow = mysqli_fetch_assoc($aNameResult)) {

                            $sTextproperty = $lLanguageID . 'Text';

                            $sRow['Name'] = $sNameRow[$lLanguageID . 'Text'];
                            $sRow['Description'] = $sNameRow[$lLanguageID . 'Description'];
                        }
                    } elseif (in_array($sTableName, $aAllowedDictionaryTables)) {

                        $sSql = "INSERT INTO Dictionary (`Key`," . $lLanguageID . "Text) VALUES('" . $sTableName . "_$sRow[Key]', '!!$sRow[Key]')";

                        $oResult = DB::$oConn->query($sSql);

                    }

                }

                if (isset($sRow['State'])) {
                    if ($sRow['State'] >= $sState) {
                        if ($sTableName == 'Dictionary') {
                            if (strpos($sRow['Key'], "Generic_") !== false) {
                                $sTempRow['Key'] = str_replace('Generic_', '', $sRow['Key']);
                                $sTempRow['Text'] = $sRow[$lLanguageID . 'Text'];
                                $sTempRow['Description'] = $sRow[$lLanguageID . 'Description'];
                                $aRows[] = $sTempRow;
                            }
                        } else {
                            if (isset($sRow['IconKey'])) {

                                $sIconSql = "SELECT Icon FROM Icon WHERE `Key` = '$sRow[IconKey]'";

                                $oIconResult = DB::$oConn->query($sIconSql);

                                if($oIconResult->num_rows > 0) {
                                    while($sIconRow = mysqli_fetch_assoc($oIconResult)) {
                                        $sRow['IconBlob'] = base64_encode($sIconRow['Icon']);
                                    }
                                }
                            }
                            $aRows[] = $sRow;
                        }
                    }
                } else {
                    if ($sTableName == 'Dictionary') {
                        if (strpos($sRow['Key'], "Generic_") !== false) {
                            $sTempRow['Key'] = str_replace('Generic_', '', $sRow['Key']);
                            $sTempRow['Text'] = $sRow[$lLanguageID . 'Text'];
                            $sTempRow['Description'] = $sRow[$lLanguageID . 'Description'];
                            $aRows[] = $sTempRow;
                        }
                    } else {
                        if (isset($sRow['IconKey'])) {

                            $sIconSql = "SELECT Icon FROM Icon WHERE `Key` = '$sRow[IconKey]'";

                            $oIconResult = DB::$oConn->query($sIconSql);

                            if($oIconResult->num_rows > 0) {
                                while($sIconRow = mysqli_fetch_assoc($oIconResult)) {
                                    $sRow['IconBlob'] = base64_encode($sIconRow['Icon']);
                                }
                            }
                        }
                        $aRows[] = $sRow;
                    }
                }
            }
        }

        array_walk_recursive($aRows, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        echo json_encode($aRows);

    } else {
        header('Content-Type: text/html; charset=utf-8');
        echo "Table Name required!";
    }
?>
