    <h1>API Docu:</h1>

    <h2>Action: GetJsonData</h2>

        <h4>Required Params:</h4>

            <div>sAction=GetJsonData</div>
            <div>sTableName=Item</div>
            <div>lLanguageID=DE  (Standard=DE)</div>

        <h4>Optional Params:</h4>

            <div>sState=2  (Standard=2)</div>
            <div>lLimit=5  (Standard=9999999999999)</div>

        <h4>Example call: <a href="http://gams.bplaced.net/api/content/GetJsonData?sTable=Item">http://gams.bplaced.net/api/content/GetJsonData?sTable=Item</a></h4>

    <h2>Action: GetMissingDics</h2>

        <h4>Required Params:</h4>

            <div>sAction=GetMissingDics</div>
            <div>sTableName=Item</div>
            <div>lLanguageID=DE  (Standard=DE)</div>

        <h4>Optional Params:</h4>

        <h4>Example call: <a href="http://gams.bplaced.net/api/content/GetMissingDics?sTable=Item">http://gams.bplaced.net/api/content/GetMissingDics?sTable=Item</a></h4>

    <h2>Action: BackupDB</h2>

        <h4>Required Params:</h4>

            <div>sAction=BackupDB</div>

        <h4>Example call: <a href="http://gams.bplaced.net/api/helper/BackupDB">http://gams.bplaced.net/api/helper/BackupDB</a></h4>
