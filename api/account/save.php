<?php
   header('Content-Type: application/json; charset=utf-8');

    if (isset($_GET['lAccountID'])) {
        $lAccountID = $_GET['lAccountID'];
    } elseif (isset($_POST['lAccountID'])) {
        $lAccountID = $_POST['lAccountID'];
    } else {
        $lAccountID = "";
    }

    if (isset($_GET['sHash'])) {
        $sHash = $_GET['sHash'];
    } elseif (isset($_POST['sHash'])) {
        $sHash = $_POST['sHash'];
    } else {
        $sHash = "";
    }

    if (isset($_GET['sSaveData'])) {
        $sSaveData = $_GET['sSaveData'];
    } elseif (isset($_POST['sSaveData'])) {
        $sSaveData = $_POST['sSaveData'];
    } else {
        $sSaveData = "";
    }

    $aJsonResult = array();

    //$sSaveData='{"ID":"30","Profiles":[{"ID":"19","Units":[{"ID":"1","UnitKey":"Druid","XP":"1001","Status":"1","Items":[]},{"ID":"19","UnitKey":"Recruit","XP":"300","Status":"1","Items":["Axe1","Fireball1","HPPotion1","Axe4"]}],"Params":{"Gold":"400","MaxGold":"10001","MaxItems":"26","MaxUnits":"11","Map_Desert1Boss1":"0","Map_Desert1DM1":"0","Map_Desert1DM2":"0","Map_Desert1POI1":"1","Map_Desert1Sub1":"0","Quest_ScavRock5":"1","Map_Desert1Town1":"0","Map_Desert1Full1":"0","Map_Desert1Temple1":"0","Quest_Desert1Spirit1":"0"},"Items":["Axe4","HPPotion1","HPPotion1"],"Buildings":[{"ID":"1","BuildingKey":"Archery","MapKey":"Desert1Boss1","Position":"1"},{"ID":"2","BuildingKey":"Arsenal","MapKey":"Desert1Boss1","Position":"2"}]}]}';

    if (!empty($sHash) && !empty($lAccountID) && !empty($sSaveData)) {

        $sAccountSql = "SELECT * FROM Account WHERE ID = $lAccountID";

        $aAccountResult = mysqli_query(DB::$oConn, $sAccountSql);

        if (is_object($aAccountResult)) {

            if($aAccountResult->num_rows > 0) {

                while($sAccountRow = mysqli_fetch_assoc($aAccountResult))
                {
                    if ($sAccountRow['Password'] == $sHash) {

                        $sSql = "SELECT ID FROM AccountProfiles WHERE AccountID = $lAccountID";

                        $oAccountResult = DB::$oConn->query($sSql);

                        $aJson = json_decode($sSaveData, true);

                        if($oAccountResult->num_rows > 0 && array_key_exists('ID', $aJson) && array_key_exists('Profiles', $aJson) && array_key_exists('ID', $aJson['Profiles'][0]) && !empty($aJson['Profiles'])) {

                            while($sAccountRow = mysqli_fetch_assoc($oAccountResult))
                            {
                                $lProfileID = $sAccountRow['ID'];

                                $sSql = "DELETE FROM ProgressItems WHERE ProfileID = $lProfileID";

                                $oResult = DB::$oConn->query($sSql);

                                $sSql = "DELETE FROM ProgressUnits WHERE ProfileID = $lProfileID";

                                $oResult = DB::$oConn->query($sSql);

                                $sSql = "DELETE FROM ProgressParams WHERE ProfileID = $lProfileID";

                                $oResult = DB::$oConn->query($sSql);

                                $sSql = "DELETE FROM ProgressBuildings WHERE ProfileID = $lProfileID";

                                $oResult = DB::$oConn->query($sSql);

                                $sSql = "DELETE FROM AccountProfiles WHERE ID = $lProfileID";

                                $oResult = DB::$oConn->query($sSql);

                            }

                            $lAccountID = $aJson['ID'];

                            if (array_key_exists('Profiles', $aJson)) {

                                $aProfiles = $aJson['Profiles'];

                                foreach ($aProfiles as $Profile) {

                                    $sProfileSql = "Insert AccountProfiles (ID, AccountID, Status) VALUES ($Profile[ID], $lAccountID, 1)";

                                    $oResult = DB::$oConn->query($sProfileSql);

                                    if (array_key_exists('Units', $Profile)) {

                                        $aUnits = $Profile['Units'];

                                        foreach ($aUnits as $Unit) {

                                            $sUnitSql = "Insert ProgressUnits (ID, ProfileID, UnitKey, XP, Status) VALUES ($Unit[ID], $Profile[ID], '$Unit[UnitKey]', $Unit[XP], $Unit[Status])";

                                            $oResult = DB::$oConn->query($sUnitSql);

                                            $aItems = $Unit['Items'];

                                            foreach ($aItems as $Item) {

                                                $sItemSql = "Insert ProgressItems (ProfileID, ProgressUnitID, ItemKey) VALUES ($Profile[ID], $Unit[ID], '$Item')";

                                                $oResult = DB::$oConn->query($sItemSql);

                                            }

                                        }

                                    }

                                    if (array_key_exists('Params', $Profile)) {

                                        $aParams = $Profile['Params'];

                                        foreach ($aParams as $key => $value) {

                                            $sParamSql = "Insert ProgressParams (ProfileID, `Key`, Value) VALUES ($Profile[ID], '$key', '$value')";

                                            $oResult = DB::$oConn->query($sParamSql);

                                        }

                                    }

                                    if (array_key_exists('Buildings', $Profile)) {

                                        $aBuildings = $Profile['Buildings'];

                                        foreach ($aBuildings as $Building) {

                                            $sBuildingSql = "Insert ProgressBuildings (ProfileID, BuildingKey, MapKey, Position) VALUES ($Profile[ID], '$Building[BuildingKey]', '$Building[MapKey]', '$Building[Position]')";

                                            $oResult = DB::$oConn->query($sBuildingSql);

                                        }

                                    }

                                    if (array_key_exists('Items', $Profile)) {

                                        $aItems = $Profile['Items'];

                                        foreach ($aItems as $Item) {

                                            $sItemSql = "Insert ProgressItems (ProfileID, ItemKey) VALUES ($Profile[ID], '$Item')";

                                            $oResult = DB::$oConn->query($sItemSql);

                                        }

                                    }

                                }

                                echo $sSaveData;
                                die();

                            }

                        } else {
                            $aJsonResult = array(
                                'Error' => clsDictionary::GetDicItem('Web_ErroInJson')
                            );
                        }

                    } else {
                        $aJsonResult = array(
                            'Error' => clsDictionary::GetDicItem('Web_WrongHash')
                        );
                    }
                }

            } else {
                $aJsonResult = array(
                    'Error' => clsDictionary::GetDicItem('Web_AccWithIDDidntExist')
                );
            }

        } else {
            $aJsonResult = array(
                'Error' => clsDictionary::GetDicItem('Web_AccWithIDDidntExist')
            );
        }

    } else {
        $aJsonResult = array(
            'Error' => clsDictionary::GetDicItem('Web_HashAndAccIDRequired')
        );
    }

    echo json_encode($aJsonResult);
?>
