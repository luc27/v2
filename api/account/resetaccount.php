<?php
    if (isset($_POST['lAccountID'])) {
        $lAccountID = $_POST['lAccountID'];
    } elseif (isset($_GET['lAccountID'])) {
        $lAccountID = $_GET['lAccountID'];
    } else {
        $lAccountID = "";
    }

    if (isset($_GET['sHash'])) {
        $sHash = $_GET['sHash'];
    } elseif (isset($_POST['sHash'])) {
        $sHash = $_POST['sHash'];
    } else {
        $sHash = "";
    }

    $aJsonResult = array();

    if (!empty($lAccountID) && !empty($sHash)) {

        $sAccountSql = "SELECT * FROM Account WHERE ID = $lAccountID";

        $aAccountResult = mysqli_query(DB::$oConn, $sAccountSql);

        if (is_object($aAccountResult)) {

            if($aAccountResult->num_rows > 0) {

                while($sAccountRow = mysqli_fetch_assoc($aAccountResult))
                {
                    if ($sAccountRow['Password'] == $sHash) {

                        resetAccount($lAccountID);

                    }
                }
            }
        }

    } else {
        $aJsonResult = array(
            'Error' => clsDictionary::GetDicItem('Web_HashAndAccIDRequired')
        );
    }

    echo json_encode($aJsonResult);
?>
