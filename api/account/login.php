<?php
    header('Content-Type: application/json; charset=utf-8');

    if (isset($_GET['sHash'])) {
        $sHash = $_GET['sHash'];
    } elseif (isset($_POST['sHash'])) {
        $sHash = $_POST['sHash'];
    } else {
        $sHash = "";
    }

    if (isset($_GET['sEMail'])) {
        $sEMail = $_GET['sEMail'];
    } elseif (isset($_POST['sEMail'])) {
        $sEMail = $_POST['sEMail'];
    } else {
        $sEMail = "";
    }

    $lAccountID = -1;
    $lProfileID = 0;
    $lUnitsIndex = 0;
    $aJsonResult = array();
    $aResult = array();
    $aProfiles = array();
    $aUnits = array();
    $aParams = array();
    $aItems = array();
    $aDefaultParams = array();
    $aBuildings = array();

    if (!empty($sHash) && !empty($sEMail)) {

        $sAccountSql = "SELECT * FROM Account WHERE Username = '$sEMail'";

        $aAccountResult = mysqli_query(DB::$oConn, $sAccountSql);

        if (is_object($aAccountResult)) {

            if($aAccountResult->num_rows > 0) {

                while($sAccountRow = mysqli_fetch_assoc($aAccountResult))
                {
                    if ($sAccountRow['Password'] == $sHash) {

                        $lAccountID = $sAccountRow['ID'];

                        $sSql = "UPDATE Account SET LastGameLogin = NOW() WHERE `ID` = $sAccountRow[ID]";

                        $oResult = DB::$oConn->query($sSql);

                        $sSql = "SELECT ID FROM AccountProfiles WHERE AccountID = $lAccountID";

                        $aResult = mysqli_query(DB::$oConn, $sSql);

                        if($aResult->num_rows > 0) {

                            while($sRow = mysqli_fetch_assoc($aResult))
                            {
                                $lProfileID = $sRow['ID'];

                                $sSqlUnits = "SELECT * FROM ProgressUnits WHERE ProfileID = $lProfileID";

                                $aResultUnit = mysqli_query(DB::$oConn, $sSqlUnits);

                                if($aResultUnit->num_rows > 0) {

                                    while($sRowUnit = mysqli_fetch_assoc($aResultUnit))
                                    {
                                        array_push($aUnits, array(
                                            'ID' => $sRowUnit['ID'],
                                            'UnitKey' => $sRowUnit['UnitKey'],
                                            'XP' => $sRowUnit['XP'],
                                            'Status' => $sRowUnit['Status'],
                                            'Items' => array()
                                        ));

                                        $sSqlItems = "SELECT * FROM ProgressItems WHERE ProfileID = $lProfileID AND ProgressUnitID = $sRowUnit[ID]";

                                        $aResultItems = mysqli_query(DB::$oConn, $sSqlItems);

                                        if($aResultItems->num_rows > 0) {

                                            while($sRowItems = mysqli_fetch_assoc($aResultItems))
                                            {
                                                array_push($aUnits[$lUnitsIndex]['Items'], $sRowItems['ItemKey']);
                                            }
                                        }

                                        $aResultItems = Array();

                                        $lUnitsIndex++;

                                    }

                                    $lUnitsIndex = 0;
                                }

                                $aResultUnit = Array();

                                $sSqlParams = "SELECT * FROM Parameter";

                                $aResultParams = mysqli_query(DB::$oConn, $sSqlParams);

                                if($aResultParams->num_rows > 0) {

                                    while($sRowParams = mysqli_fetch_assoc($aResultParams))
                                    {
                                        $aDefaultParams = array_merge($aDefaultParams, array($sRowParams['Key'] => $sRowParams['Default']));
                                    }
                                }

                                $sSqlParams = "SELECT * FROM Map";

                                $aResultParams = mysqli_query(DB::$oConn, $sSqlParams);

                                if($aResultParams->num_rows > 0) {

                                    while($sRowParams = mysqli_fetch_assoc($aResultParams))
                                    {
                                        $aDefaultParams = array_merge($aDefaultParams, array('Map_' . $sRowParams['Key'] => 0));
                                    }
                                }

                                $sSqlParams = "SELECT * FROM Town";

                                $aResultParams = mysqli_query(DB::$oConn, $sSqlParams);

                                if($aResultParams->num_rows > 0) {

                                    while($sRowParams = mysqli_fetch_assoc($aResultParams))
                                    {
                                        $aDefaultParams = array_merge($aDefaultParams, array('Town_' . $sRowParams['Key'] => 0));
                                    }
                                }

                                $sSqlParams = "SELECT * FROM Quest";

                                $aResultParams = mysqli_query(DB::$oConn, $sSqlParams);

                                if($aResultParams->num_rows > 0) {

                                    while($sRowParams = mysqli_fetch_assoc($aResultParams))
                                    {
                                        $aDefaultParams = array_merge($aDefaultParams, array('Quest_' . $sRowParams['Key'] => 0));
                                    }
                                }

                                $sSqlParams = "SELECT * FROM ProgressParams WHERE ProfileID = $lProfileID";

                                $aResultParams = mysqli_query(DB::$oConn, $sSqlParams);

                                if($aResultParams->num_rows > 0) {

                                    while($sRowParams = mysqli_fetch_assoc($aResultParams))
                                    {
                                        $aParams = array_merge($aParams, array($sRowParams['Key'] => $sRowParams['Value']));
                                    }
                                }

                                if (is_array($aParams)) {
                                    $aParams = $aParams + $aDefaultParams;
                                } else {
                                    $aParams = $aDefaultParams;
                                }

                                $sSqlItems = "SELECT * FROM ProgressItems WHERE ProfileID = $lProfileID AND ProgressUnitID IS NULL";

                                $aResultItems = mysqli_query(DB::$oConn, $sSqlItems);

                                if($aResultItems->num_rows > 0) {

                                    while($sRowItems = mysqli_fetch_assoc($aResultItems))
                                    {
                                        array_push($aItems, $sRowItems['ItemKey']);
                                    }
                                }

                                $sSqlBuildings = "SELECT * FROM ProgressBuildings WHERE ProfileID = $lProfileID";

                                $aResultBuildings = mysqli_query(DB::$oConn, $sSqlBuildings);

                                if($aResultBuildings->num_rows > 0) {

                                    while($sRowBuilding = mysqli_fetch_assoc($aResultBuildings))
                                    {
                                        array_push($aBuildings, array(

                                            'ID' => $sRowBuilding['ID'],
                                            'BuildingKey' => $sRowBuilding['BuildingKey'],
                                            'MapKey' => $sRowBuilding['MapKey'],
                                            'Position' => $sRowBuilding['Position']
                                        ));
                                    }
                                }

                                array_push($aProfiles, array(
                                    'ID' => $lProfileID,
                                    'Units' => $aUnits,
                                    'Params' => $aParams,
                                    'Items' => $aItems,
                                    'Buildings' => $aBuildings
                                ));

                                $aUnits = Array();
                                $aParams = Array();
                            }
                        }

                        $aJsonResult = array(
                            'ID' => $lAccountID,
                            'Profiles' => $aProfiles
                        );

                        array_walk_recursive($aJsonResult, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                            $item = utf8_encode($item);
                        });

                    } else {
                        $aJsonResult = array(
                            'Error' => clsDictionary::GetDicItem('Web_WrongPassword')
                        );
                    }
                }

            } else {
                $aJsonResult = array(
                    'Error' => clsDictionary::GetDicItem('Web_AccountNotExists')
                );
            }

        } else {
            $aJsonResult = array(
                'Error' => clsDictionary::GetDicItem('Web_AccountNotExists')
            );
        }

    } else {
        $aJsonResult = array(
            'Error' => clsDictionary::GetDicItem('Web_HashAndEmailReq')
        );
    }

    echo json_encode($aJsonResult);
 ?>
