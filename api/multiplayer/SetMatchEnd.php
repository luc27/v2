<?php
    header('Content-Type: application/json; charset=utf-8');

    if (isset($_GET['lAccountID'])) {
        $lAccountID = $_GET['lAccountID'];
    } elseif (isset($_POST['lAccountID'])) {
        $lAccountID = $_POST['lAccountID'];
    } else {
        $lAccountID = "";
    }

    if (isset($_GET['lStatus'])) {
        $lStatus = $_GET['lStatus'];
    } elseif (isset($_POST['lStatus'])) {
        $lStatus = $_POST['lStatus'];
    } else {
        $lStatus = "";
    }

    if (!empty($lAccountID) && !empty($lStatus)) {

        if ($lStatus > 0) {

            $sSql = "SELECT Player2 FROM `Match` WHERE AccountID = $lAccountID";

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($sRow = mysqli_fetch_assoc($oResult))
                {
                    $lAccountIDPlayer2 = $sRow['Player2'];
                }

            }

            if (is_numeric($lAccountIDPlayer2)) {
                if ($lStatus == 1) {
                    $lWinnerID = $lAccountID;
                    $lLoserID = $lAccountIDPlayer2;
                } elseif ($lStatus == 2) {
                    $lLoserID = $lAccountID;
                    $lWinnerID = $lAccountIDPlayer2;
                }

                $sMapSql = "Insert MatchResults (Winner, Loser) VALUES ($lWinnerID, $lLoserID)";

                $oResult = DB::$oConn->query($sMapSql);
            }

        }

        $sSql = "DELETE FROM `Match` WHERE AccountID = $lAccountID";

        $oResult = DB::$oConn->query($sSql);

        $aJsonResult = array(
            'Success' => clsDictionary::GetDicItem('Web_MatchClosed')
        );

    } else {
        $aJsonResult = array(
            'Error' => clsDictionary::GetDicItem('Web_MatchEndParamsReq')
        );
    }

    echo json_encode($aJsonResult);
 ?>
