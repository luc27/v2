<?php
    header('Content-Type: application/json; charset=utf-8');

    $aRows = array();

    $sSql = "SELECT * FROM `Match`";

    $oResult = DB::$oConn->query($sSql);

    if (is_object($oResult)) {

        if($oResult->num_rows > 0) {

            while($sRow = mysqli_fetch_assoc($oResult))
            {
                $aRows[] = $sRow;
            }

        }

    }

    array_walk_recursive($aRows, function (&$item, $key) {
        $item = null === $item ? '' : $item;
    });

    echo json_encode($aRows);
 ?>
