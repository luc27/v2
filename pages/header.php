 <!DOCTYPE html><html>

    <head>

        <title>Vikings</title>

        <link rel="stylesheet" href="/default/css/default.css" media="screen" title="no title">
        <link rel="stylesheet" href="/default/css/module.css" media="screen" title="no title">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        
        <script src="/default/js/default.js"></script>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex, nofollow">

    </head>

    <body>

        <div class="innerbody">

            <header class="pageheader">
<?php
    if (bLoggedIn) {

        $oAccount = new clsAccount();
        $oAccount->lAccountID = $_SESSION['lAccountID'];
        $oAccount->Load();
        
        $oProfile = new clsProfile();
        $oProfile->sQueryType = "byProfileID";
        $oProfile->lProfileID = $_SESSION['lProfileID'];
        $oProfile->Load();
?>
                <div class="grid">

                    <?=$oAccount->GetAccountName();?>
                    <a class="button1" href="/game/home">/home</a>
                    <a class="button1" href="/account/settings">/settings</a>
                    <a class="button1" href="/account/logout-send">/logout</a>
                
                </div>
                
                <div class="floordata">
                
                    <div>Gold: <?=$oProfile->GetProfileGold();?>/<?=$oProfile->GetMaxGold();?></div>
                    <div>Items: <?=$oProfile->GetItems();?>/<?=$oProfile->GetMaxItems();?></div>
                    <div>Crew: <?=$oProfile->GetUnits();?>/<?=$oProfile->GetMaxUnits();?></div>
                
                </div>
<?php
    }
?>
            </header>

            <div class="content">

                <div class="innercontent">
