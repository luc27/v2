<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $oMatch = new clsMatch();
    $oMatch->sQueryType = "byAccountID";
    $oMatch->lAccountID = $_SESSION['lAccountID'];
    $oMatch->Load();
?>
    <div class="matches centercontent">
    
        <div class="list">
<?php
                if($oMatch->Eof()) {
                    echo("No current matches");
                }
                $oAccount = new clsAccount();
                while(!$oMatch->Eof()) {
                
                    $bAttacker = $oMatch->GetAccountID() == $oMatch->lAccountID;
                    if ($bAttacker) {
                        $oAccount->lAccountID = $oMatch->GetAccountID2();
                    } else {
                        $oAccount->lAccountID = $oMatch->GetAccountID();
                    }
                    $oAccount->Load();
                    
                    $sLink = "/game/match?m=" . $oMatch->GetID();
                    if ($oMatch->IsSetup()) $sLink = "/game/setmatch?m=" . $oMatch->GetID();
?>
            <a class="button1" href="<?=$sLink?>">
                Match <?=$oMatch->GetID();?> against <?=$oAccount->GetAccountName()?>
                <span class="sub"><?=$oMatch->GetStatusText()?></span>
            </a>
<?php
                    $oMatch->MoveNext();
                }
?>
        </div>
    
    </div>