<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $oProfile = new clsProfile();
    $oProfile->sQueryType = "byProfileID";
    $oProfile->lProfileID = $_SESSION['lProfileID'];
    $oProfile->Load();
    
    $lMaxUnits = $oProfile->GetMaxUnits();
    
    $oUnit = new clsUnit();
    $oUnit->sQueryType = "byProfileID";
    $oUnit->lProfileID = $oProfile->GetProfileID();
    $oUnit->Load();
    
    $lUnits = 0;
?>
    <div class="crew centercontent">
    
        <div class="units grid">
<?php
                while(!$oUnit->Eof()) {
?>
            <div data-status="<?=$oUnit->GetUnitStatusText()?>" data-id="<?=$oUnit->GetUnitID()?>" class="unit unitcard clearafter">

                <div class="headerbar"><img src="/default/images/design/unit.png" alt=""><?=$oUnit->GetUnitKey();?></div>

                <div class="hpbar outerbar" data-value="<?=$oUnit->GetCurrentHP();?>/<?=$oUnit->GetUnitHP();?>">
                    <span class="bar" style="width: <?=$oUnit->GetCurrentHP() / $oUnit->GetUnitHP() * 100;?>%;">&nbsp;</span>
                </div>
                <div class="apbar outerbar" data-value="<?=$oUnit->GetCurrentAP();?>/<?=$oUnit->GetUnitAP();?>">
                    <span class="bar" style="width: <?=$oUnit->GetCurrentAP() / $oUnit->GetUnitAP() * 100;?>%;">&nbsp;</span>
                </div>

                <div class="xp">XP: <?=$oUnit->GetUnitXP();?></div>

                <div class="level">Level: <?=$oUnit->GetUnitLevel();?></div>

                <div class="phys">Phys: <?=$oUnit->GetUnitPhysAttackPower();?></div>

                <div class="magic">Mag: <?=$oUnit->GetUnitMagicAttackPower();?></div>

                <div class="def">Def: <?=$oUnit->GetUnitDefense();?></div>

                <div class="gearscore">GS: <?=$oUnit->GetUnitGearScore();?></div>
<?php
                    $oItem = new clsItem();
                    $oItem->sQueryType = "byUnitID";
                    $oItem->lUnitID = $oUnit->GetUnitID();
                    $oItem->Load();

                    if (!$oItem->Eof()) {
?>
                        <div class="items">
<?php
                        while(!$oItem->Eof()) {
?>
                            <div class="item clearafter">

                                <div class="itemkey"><?=$oItem->GetItemKey();?></div>

                            </div>
<?php
                            $oItem->MoveNext();
                        }
?>
                        </div>
<?php
                    }

                    if ($oUnit->GetUnitStatus() == 2) {
?>
                        <div class="rezzbutton cta ctagreen" onclick="oUnit.rezz('<?=$oProfile->GetProfileID();?>', '<?=$oUnit->GetUnitID();?>');"><img src="/default/images/design/rezz.png" alt="">Rezz for <?=$oUnit->GetRezzCosts()?></div>
<?php
                    }
                    
                    if ($oUnit->GetUnitStatus() == 0 && $oUnit->GetCurrentHP() < $oUnit->GetUnitHP()) {
?>
                        <div class="healbutton cta ctagreen" onclick="oUnit.heal('<?=$oProfile->GetProfileID();?>', '<?=$oUnit->GetUnitID();?>');"><img src="/default/images/design/rezz.png" alt="">Heal for <?=$oUnit->GetHealingCosts()?></div>
<?php
                    }
                    
                    if ($oUnit->GetUnitStatus() == 0) {
?>
                        <div class="sellbutton cta ctared" onclick="if (UI.Confirm()) oUnit.dismiss('<?=$oUnit->GetUnitID();?>');">Dismiss</div>
<?php
                    }
                    
                    $lUnits ++;
                    
                    $oUnit->MoveNext();
?>
            </div>
<?php
                }
                
                if ($lUnits < $lMaxUnits) {
?>
            <a href="/api/game/AddUnit" class="button button1 addbutton">Add new Recruit</a>
<?php
                }
?>
        </div>
    
    </div>
