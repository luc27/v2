<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $lMatchID = $_GET['m'];
    
    $oProfile = new clsProfile();
    $oProfile->lAccountID = $_SESSION['lAccountID'];
    $oProfile->Load();
    
    $oUnit = new clsUnit();
    $oUnit->sQueryType = "byProfileID";
    $oUnit->lProfileID = $oProfile->GetProfileID();
    $oUnit->Load();
?>
    <div class="setmatch centercontent">
    
        <div class="units grid">
<?php
                while(!$oUnit->Eof()) {
?>
            <div data-status="<?=$oUnit->GetUnitStatusText()?>" data-id="<?=$oUnit->GetUnitID()?>" class="unit unitcard clearafter">

                <div class="image"><img src="/default/images/design/unit.png" alt=""><?=$oUnit->GetUnitKey();?></div>

                <div class="hp">HP: <?=$oUnit->GetCurrentHP();?></div>

                <div class="xp">XP: <?=$oUnit->GetUnitXP();?></div>

                <div class="level">Level: <?=$oUnit->GetUnitLevel();?></div>

                <div class="gearscore">GS: <?=$oUnit->GetUnitGearScore();?></div>

            </div>
<?php
                    $oUnit->MoveNext();
                }
?>
        </div>
        
        <div class="clear">&nbsp;</div>
        
        <a onclick="setCrew();" class="button button1">setCrew</a>
        
        <script type="text/javascript">// <![CDATA[
        
            var selectedClass = 'selected';
            
            function setCrew () {
                var sSelectedUnits = $('.setmatch .unit.' + selectedClass).map(function () {
                    return $(this).data('id');
                }).get().join();
                
                $.ajax('/api/game/SetMatchMember', {
                    type: 'POST',
                    data: {
                        'MatchID': '<?=$lMatchID?>',
                        'Units': sSelectedUnits
                    },
                    success: $.proxy(function(data){
                        if (data.Status == 1) {
                          location.href = '/game/match?m=<?=$lMatchID?>';
                        } else {
                            alert(data.Error);
                        }
                    }, this)
                });
            }
            
            $('.setmatch .unit').click(function () {
                if ($(this).hasClass(selectedClass)) {
                    $(this).removeClass(selectedClass);
                } else {
                    if ($(this).data('status') != 'Ready') {
                        // error;
                    } else {
                        $(this).addClass(selectedClass);
                    }
                }
            });
        
        // ]]></script>
    
    </div>
