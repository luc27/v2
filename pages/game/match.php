<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $lMatchID = $_GET['m'];
    $lAccountID = $_SESSION['lAccountID'];
    
    $oMatch = new clsMatch();
    $oMatch->sQueryType = "byID";
    $oMatch->lMatchID = $lMatchID;
    $oMatch->Load();
    
    $bAttacker = $oMatch->GetAccountID() == $lAccountID;
    $bDefendand = $oMatch->GetAccountID2() == $lAccountID;
    
    if (!$bAttacker && !$bDefendand) {
        $oPageConfig->To('/game/matches');
    } else if ($oMatch->IsWon()) {
?>
    <div class="match centercontent">
    
        <h3>WON!</h3>
        <a href="/api/game/collect?m=<?=$lMatchID?>" class="button button1">Collect <?=$oMatch->GetOwnPot();?></a>
    
    </div>
<?php
    } else if ($oMatch->IsOver()) {
        $oPageConfig->To('/game/matches');
    } else {
        $oUnit = new clsUnit();
        $oUnit->sQueryType = "byMatchID";
        $oUnit->lMatchID = $lMatchID;
        $oUnit->Load();
?>
    <div class="match centercontent">
    
        <div class="units grid">
        
            <div class="dividerline"></div>
<?php
                while(!$oUnit->Eof()) {
?>
            <div class="unit unitcard clearafter"<?if ($oUnit->IsOwn()) {?> data-singleselect="unit" data-own<?}?> data-id="<?=$oUnit->GetUnitID()?>">

                <div class="headerbar"><img src="/default/images/design/unit.png" alt=""><?=$oUnit->GetUnitKey();?></div>

                <div class="hpbar outerbar" data-value="<?=$oUnit->GetCurrentHP();?>/<?=$oUnit->GetUnitHP();?>">
                    <span class="bar" style="width: <?=$oUnit->GetCurrentHP() / $oUnit->GetUnitHP() * 100;?>%;">&nbsp;</span>
                </div>
                <div class="apbar outerbar" data-value="<?=$oUnit->GetCurrentAP();?>/<?=$oUnit->GetUnitAP();?>">
                    <span class="bar" style="width: <?=$oUnit->GetCurrentAP() / $oUnit->GetUnitAP() * 100;?>%;">&nbsp;</span>
                </div>
<?php
                    $oAura = new clsAura();
                    $oAura->sQueryType = "byUnitID";
                    $oAura->lUnitID = $oUnit->GetUnitID();
                    $oAura->Load();
                    
                    if(!$oAura->Eof()) {
?>
                <div class="auras">
<?php
                      while(!$oAura->Eof()) {
?>
                    <div><?=$oAura->GetKey();?>(<?=$oAura->GetRestTurns();?> more Turns)</div>
<?php
                            $oAura->MoveNext();
                        }
?>
                </div>
<?php
                    }
                    
                    if ($oMatch->IsOwnTurn()) {
                    
                        $oSpell = new clsSpell();
                        $oSpell->sQueryType = "byUnitID";
                        $oSpell->lUnitID = $oUnit->GetUnitID();
                        $oSpell->Load();
                        
                        if(!$oSpell->Eof()) {
?>
                <div class="unitactions list" style="display: none;">
<?php
                        while(!$oSpell->Eof()) {
?>
                    <a 
                        data-activationtype="<?=$oSpell->GetActivationType();?>" 
                        data-action="<?=$oSpell->GetSpellKey();?>" 
                        data-singleselect="action" 
                        class="cta ctablue border<?=$oSpell->GetActivationType();?>"
                    ><?=$oSpell->GetSpellKey();?></a>
<?php
                                $oSpell->MoveNext();
                            }
?>
                </div>
<?php
                        }
                    }
?>
            </div>
<?php
                    $oUnit->MoveNext();
                }
?>
        </div>
<?php
                if ($oMatch->IsOwnTurn()) {
?>
        <div class="teamactions grid">
            <a class="button1" href="/api/game/EndTurn?m=<?=$lMatchID?>">End Turn</a>
        </div>
        
        <script type="text/javascript">// <![CDATA[
        
            $(function () {
                function selectSelf () {
                    enemies.removeClass('selectable');
                    crew.addClass('selectable');
                }
                
                function selectTarget () {
                    enemies.addClass('selectable');
                    crew.removeClass('selectable');
                }
                
                var enemies = $('.match .units .unit:not([data-own])');
                var enemieyRows = $('.match .units .rows');
                var crew = $('.match .units .unit[data-own]');
                
                selectSelf();
                
                $(window).on('game:selectionchange', function () {
                    if (Game.locked)
                        return;
                    
                    var currentUnit = Game.currentSelected.unit();
                    var currentAction = Game.currentSelected.action();
                    
                    $('.unitactions')[(currentUnit.length > 0 ? 'slideDown' : 'slideUp')]();
                    
                    if (currentUnit.length && currentAction.length) {
                        var action = currentAction.data('action');
                        var unit = currentUnit.data('id');
                        switch (currentAction.data('activationtype')) {
                            case 1: // instant
                                Game.locked = true;
                                oUnit.attack(action, unit);
                                break;
                        
                            case 3: // target row
                                selectTarget();
                                enemieyRows.one('click', function () {
                                    Game.locked = true;
                                    oUnit.attack(action, unit, $(this).data('id'));
                                    selectSelf();
                                });
                                break;
                            default: // case 2: // target unit
                                selectTarget();
                                enemies.one('click', function () {
                                    Game.locked = true;
                                    oUnit.attack(action, unit, $(this).data('id'));
                                    selectSelf();
                                });
                                break;
                        }
                    }
                });
            });
         
        // ]]></script>
<?php
                } else {
?>
        <p>Enemy's turn</p>
<?php
                }
?>
    </div>
<?php
    }
?>