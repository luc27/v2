<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $oAccount = new clsAccount();
    $oAccount->sQueryType = "select";
    $oAccount->Load();
?>
    <div class="map">

        <div class="grid">
<?php
                while(!$oAccount->Eof()) {
                    if ($oAccount->GetAccountID() != $_SESSION['lAccountID']) {
?>
            <a class="button1" onclick="getSetMatch('<?=$oAccount->GetAccountID();?>');"><?=$oAccount->GetAccountName();?></a>
<?php
                    }
                    $oAccount->MoveNext();
                }
?>
        </div>
        <script type="text/javascript">// <![CDATA[
        
            function getSetMatch (lAccountIDPlayer2) {
                $.ajax('/api/game/SetMatch', {
                    type: 'POST',
                    data: {
                        'MapKey': 'Arena1on1',
                        'lAccountIDPlayer2': lAccountIDPlayer2
                    },
                    success: $.proxy(function(data){
                        if (data.Status == 1) {
                          location.href = '/game/setmatch?m=' + data.Created;
                        } else {
                            alert(data.Error);
                        }
                    }, this)
                });
            }
        
        // ]]></script>
    
    </div>
