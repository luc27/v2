<?php
    $oPageConfig->SecureThisPage('/account/login');
    
    $oMap = new clsMap();
    $oMap->sQueryType = "select";
    $oMap->Load();
?>
    <div class="map">

        <div class="grid">
<?php
                while(!$oMap->Eof()) {
?>
            <a class="button1" onclick="getSetMatch('<?=$oMap->GetKey();?>');"><?=$oMap->GetKey();?></a>
<?php
                    $oMap->MoveNext();
                }
?>
        </div>
        <script type="text/javascript">// <![CDATA[
        
            function getSetMatch (MapKey) {
                $.ajax('/api/game/SetMatch', {
                    type: 'POST',
                    data: {
                        'MapKey': MapKey
                    },
                    success: $.proxy(function(data){
                        if (data.Status == 1) {
                          location.href = '/game/setmatch?m=' + data.Created;
                        } else {
                            alert(data.Error);
                        }
                    }, this)
                });
            }
        
        // ]]></script>
    
    </div>
