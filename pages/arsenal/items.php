<?php
    $oItem = new clsItem();
    $oItem->Load();

    if (!$oItem->Eof()) {
?>
    <div class="arsenal items centercontent">

        <div class="introduction">

            <div class="headline t4">Items</div>

        </div>

        <div class="elements">
<?php
        while(!$oItem->Eof()) {
?>
            <div class="element">

                <div class="title inverse small"><?=$oItem->GetItemName();?></div>

            </div>
<?php
            $oItem->MoveNext();

        }
?>
        </div>

    </div>
<?php
    }
?>
