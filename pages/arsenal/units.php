<?php
    $oUnit = new clsUnit();
    $oUnit->Load();

    if (!$oUnit->Eof()) {
?>
    <div class="arsenal units centercontent">

        <div class="introduction">

            <div class="headline t4">Units</div>

        </div>

        <div class="elements">
<?php
        while(!$oUnit->Eof()) {
?>
            <div class="element">

                <div class="title inverse small"><?=$oUnit->GetUnitName();?></div>

            </div>
<?php
            $oUnit->MoveNext();

        }
?>
        </div>

    </div>
<?php
    }
?>
