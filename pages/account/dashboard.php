<?php
    $oPageConfig->SecureThisPage('/account/login');
?>
    <div class="dashboard centercontent">

        <div class="profiles clearafter">
<?php
    $oProfile = new clsProfile();
    $oProfile->lAccountID = $_SESSION['lAccountID'];
    $oProfile->Load();

    if (!$oProfile->Eof()) {
?>
            <div class="headline">Profiles:</div>
<?php
        while(!$oProfile->Eof()) {
?>
            <div class="profile">

                <div class="stats">

                    <div class="id">ID: <?=$oProfile->GetProfileID();?></div>

                    <div class="status">Status: <?=$oProfile->GetProfileStatus();?></div>

                    <div class="gold">Gold: <?=$oProfile->GetProfileGold();?></div>

                </div>
<?php
            $oUnit = new clsUnit();
            $oUnit->sQueryType = "byProfileID";
            $oUnit->lProfileID = $oProfile->GetProfileID();
            $oUnit->Load();

            if (!$oUnit->Eof()) {
?>
                <div class="units">

                    <div>Units:</div>
<?php
                while(!$oUnit->Eof()) {
?>
                    <div class="unit clearafter">

                        <div class="stats clearafter">

                            <div class="image"><img src="/default/images/design/unit.png" alt=""><?=$oUnit->GetUnitKey();?></div>

                            <div class="status">Status: <?=$oUnit->GetUnitStatusText();?></div>

                            <div class="xp">XP: <?=$oUnit->GetUnitXP();?></div>

                            <div class="level">Level: <?=$oUnit->GetUnitLevel();?></div>

                            <div class="hp">HP: <?=$oUnit->GetUnitHP();?></div>

                            <div class="ap">AP: <?=$oUnit->GetUnitAP();?></div>

                            <div class="phys">Phys: <?=$oUnit->GetUnitPhysAttackPower();?></div>

                            <div class="magic">Mag: <?=$oUnit->GetUnitMagicAttackPower();?></div>

                            <div class="def">Def: <?=$oUnit->GetUnitDefense();?></div>

                            <div class="gearscore">GS: <?=$oUnit->GetUnitGearScore();?></div>

                        </div>
<?php
                    $oItem = new clsItem();
                    $oItem->sQueryType = "byUnitID";
                    $oItem->lUnitID = $oUnit->GetUnitID();
                    $oItem->Load();

                    if (!$oItem->Eof()) {
?>
                        <div class="title">Items:</div>

                        <div class="items">
<?php
                        while(!$oItem->Eof()) {
?>
                            <div class="item clearafter">

                                <div class="itemkey"><?=$oItem->GetItemKey();?></div>

                            </div>
<?php
                            $oItem->MoveNext();
                        }
?>
                        </div>
<?php
                    }

                    if ($oUnit->GetUnitStatus() == 2) {
?>
                        <div class="rezzbutton cp" onclick="oUnit.rezz('<?=$oProfile->GetProfileID();?>', '<?=$oUnit->GetUnitID();?>');"><img src="/default/images/design/rezz.png" alt="">Rezz</div>
<?php
                    }
?>
                    </div>
<?php
                    $oUnit->MoveNext();
                }
?>
                </div>
<?php
            }
?>
            </div>
<?php
            $oProfile->MoveNext();

        }

    }
?>
        </div>

    </div>
