<?php
    if (bLoggedIn) {
        header('Location: /account/dashboard');
        die();
    }

    $sError = @$_GET['sError'];
 ?>
    <div class="login centercontent tac">

        <h4>Login:</h4>

        <form class="loginform" action="/account/login-send" method="post">

            <div class="username">

                <div class="text">Username</div>

                <input type="text" name="username" value="">

            </div>

            <div class="password">

                <div class="text">Password</div>

                <input type="password" name="password" value="">

            </div>

             <div class="error"><?=$sError?></div>

             <input class="button button1" type="submit" value="Login">

        </form>

        <div class="registration"><a href="/account/registration">Noch nicht registriert?</a></div>

        <div class="password"><a href="/account/passwordrecover">Passwort vergessen?</a></div>

    </div>
