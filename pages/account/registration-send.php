<?php
    $sUserName = $_POST['username'];
    $sEmail = $_POST['email'];
    $sFirstName = $_POST['firstname'];
    $sLastName = $_POST['lastname'];
    $sPassword = $_POST['password'];
    $sPasswordCheck = $_POST['passwordcheck'];

    if ($sUserName != '' && $sEmail != '' && $sPassword != '' && $sPasswordCheck != '' && $sFirstName != '' && $sLastName != '') {

        if ($sPassword != $sPasswordCheck) {
            header('Location: /account/registration?sError=The passwords do not match!');
            die();
        }

        $sPassword = $oPageConfig->GetMd5($sPassword);

        $oResult = DB::$oConn->query("CALL CreateAccount('$sUserName', '$sPassword', '$sFirstName', '$sLastName', '$sEmail')");

        if (DB::$oConn->error != '') {
            header('Location: /account/registration?sError= ' . DB::$oConn->error);
            die();
        } else {

            $sSql = "SELECT ID FROM Account WHERE LOWER(Username) = '" . strtolower($sUserName) . "'";

            $aResult = mysqli_query(DB::$oConn, $sSql);

            if($aResult->num_rows > 0) {

                while($sRow = mysqli_fetch_assoc($aResult))
                {
                    $sSql = "UPDATE Account SET CreationDate = NOW() WHERE `ID` = $sRow[ID]";

                    $oResult = DB::$oConn->query($sSql);

                    $sSql = "INSERT INTO AccountProfiles (AccountID, Status) VALUES ($sRow[ID], '1')";

                    $aResult = mysqli_query(DB::$oConn, $sSql);

                    $lProfileID = DB::$oConn->insert_id;

                    $sSql = "INSERT INTO ProgressUnits (ProfileID, UnitKey, XP, Status) VALUES ($lProfileID, 'Recruit', '0', 1)";

                    $aResult = mysqli_query(DB::$oConn, $sSql);

                    $lProgressUnitID = DB::$oConn->insert_id;

                    $sSql = "INSERT INTO ProgressItems (ProfileID, ProgressUnitID, ItemKey) VALUES ($lProfileID, $lProgressUnitID, 'HPPotion1')";

                    $aResult = mysqli_query(DB::$oConn, $sSql);

                }
            }
        }

        header('Location: /account/registration-thanks');
        die();
    }
?>
