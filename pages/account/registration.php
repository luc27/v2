<?php
    $sError = @$_GET['sError'];
 ?>
    <div class="registration centercontent tac">

        <h3>Registrierung:</h3>

        <form class="loginform" action="/account/registration-send" method="post">

            <div class="firstname">

                <div class="text">Vorname</div>

                <input type="text" name="firstname" value="" required>

            </div>

            <div class="lastname">

                <div class="text">Nachname</div>

                <input type="text" name="lastname" value="" required>

            </div>

            <div class="email">

                <div class="text">EMail</div>

                <input type="text" name="email" value="" required>

            </div>

            <div class="username">

                <div class="text">Username</div>

                <input type="text" name="username" value="" required>

            </div>

            <div class="password">

                <div class="text">Password</div>

                <input type="password" name="password" value="" required>

            </div>

            <div class="passwordcheck">

                <div class="text">Passwort bestätigen</div>

                <input type="password" name="passwordcheck" value="" required>

            </div>

             <input class="button button1" type="submit" value="Registrieren">

             <div class="error"><?=$sError?></div>

        </form>

    </div>
