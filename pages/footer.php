
                </div>

            </div>
            
            <script>// <![CDATA[
            
                $(function () {
                
                    $('[data-singleselect]').click(function () {
                        var siblings = $('[data-singleselect="' + $(this).data('singleselect') + '"]').not(this);
                        
                        siblings.removeClass('selected');
                        $(this).toggleClass('selected');
                        
                        $(window).trigger('game:selectionchange');
                    });
                    
                    Game.currentSelected.unit = function () { return Game.currentSelected('unit') };
                    Game.currentSelected.action = function () { return Game.currentSelected('action') };
                
                });
            
            // ]]></script>
            
            <? if (false) { ?>
            <footer class="pagefooter">

                <div class="centercontent inverse small">

                    <div class="element copyright">&copy; <?=date("Y");?> Gams GmbH</div>

                    <div class="element privacy"><a href="/information/privacy">Privacy</a></div>

                    <div class="element sitemap"><a href="/information/sitemap">Sitemap</a></div>

                </div>

            </footer>
            <? } ?>

        </div>

    </body>

</html>
