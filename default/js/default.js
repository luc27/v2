
    oAccount = {

        reset: function () {

            $.ajax({
                type: 'POST',
                url: '/ajax/resetaccount',
                data: 'bAjax=true',
                dataType: 'text'
            }).done(function (data) {
                location.href = '/account/dashboard';
            });

        },

        delete: function () {

            $.ajax({
                type: 'POST',
                url: '/ajax/deleteaccount',
                data: 'bAjax=true',
                dataType: 'text'
            }).done(function (data) {
                location.href = '/account/logout-send';
            });

        }

    };
    
    Game = {
        locked: false,
        currentSelected: function (sType) {
        
            return $('[data-singleselect="' + sType + '"].selected');
        
        }
    }
    
    UI = {
        Confirm: function (msg) {
            return confirm(msg);
        }
    }
    
    oUnit = {

        rezz: function (lProfileID, lUnitID) {

            $.ajax({
                type: 'POST',
                url: '/api/game/RezzUnit',
                data: 'lProfileID=' + lProfileID + '&lUnitID=' + lUnitID,
                dataType: 'text'
            }).done(function (data) {
                location.reload();
            });

        },
        
        heal: function (lProfileID, lUnitID) {

            $.ajax({
                type: 'POST',
                url: '/api/game/healunit',
                data: 'lProfileID=' + lProfileID + '&lUnitID=' + lUnitID,
                dataType: 'text'
            }).done(function (data) {
                location.reload();
            });

        },
        
        attack: function (sAttackKey, lUnitIDFrom, lUnitIDTo) {

            $.ajax({
                type: 'POST',
                url: '/api/game/attack',
                data: 'sAttackKey=' + sAttackKey + '&lUnitIDFrom=' + lUnitIDFrom + '&lUnitIDTo=' + lUnitIDTo,
                dataType: 'text'
            }).done(function (data) {
                location.reload();
            });

        },
        
        dismiss: function (lUnitID) {

            $.ajax({
                type: 'POST',
                url: '/api/game/dismissunit',
                data: 'lUnitID=' + lUnitID,
                dataType: 'text'
            }).done(function (data) {
                location.reload();
            });

        }

    };
    
