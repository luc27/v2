<?php
    /*

    Example/Test call:

    $oWoWApi = new clsWorldOfWarcraft();
    $oWoWApi->sDataType = 'character';
    $oWoWApi->AddParam('eredar');
    $oWoWApi->AddParam('Varhius');
    $oWoWApi->Load();

    if (!$oWoWApi->Eof()) {
        echo $oWoWApi->GetData('name');
    }

    */

    class clsCloudBuild
    {
        public $sQueryType;
        public $lOrgID;
        public $lProjectID;
        public $lBuildTargetID;
        public $lNumberID;

        private $sApiKey;
        private $sBasePath;
        private $sResponseData;

        function __construct() {

            $this->sQueryType = "GetAllBuilds";
            $this->lOrgID = "gams-e37b10a7-93d6-4497-b6c6-c4d5a5ffc1d5";
            $this->lProjectID = "vikings1";
            $this->lBuildTargetID = "default-windows-desktop-64-bit";
            $this->lNumberID = "";

            $this->sApiKey = '8854004de68620ebef7e7497667613f3';
            $this->sBasePath = 'https://build-api.cloud.unity3d.com/api/v1/';
            $this->sResponseData = '';

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                default:

                    $sApiPath = "orgs/" . $this->lOrgID . "/projects/" . $this->lProjectID . "/buildtargets/" . $this->lBuildTargetID . "/builds";

                    break;
            }

            $opts = array(
              'http'=>array(
                'method'=>"GET",
                'header' => 'Authorization: Basic ' . $this->sApiKey
              )
            );

            $context = stream_context_create($opts);

            $sResponse = @file_get_contents($this->sBasePath . $sApiPath, false, $context);

            if (strpos($http_response_header[0], '200')) {

                $this->sResponseData = json_decode($sResponse, true);

            } else {

                echo "Error in Json:" . $http_response_header[0];

            }

        }

        public function GetData (){

            return $this->sResponseData;

        }

        public function Eof () {

            return MyIF(is_array($this->sResponseData), false, true);

        }

    }
?>
