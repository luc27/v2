<?php
    class clsMap
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $sMapKey;
        
        function __construct() {

            $this->sQueryType = "select";

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                case "bykey":

                    $sSql = "SELECT * FROM Map WHERE `Key` = '$this->sMapKey'";

                    break;

                case "select":

                    $sSql = "SELECT * FROM Map";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);
            
            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function GetID () { return -1; }
        public function GetKey () {
        
            if (array_key_exists('Key', current($this->aRows))) {
                return current($this->aRows)['Key'];
            } else {
                return '';
            }

        }

        public function GetAccountID () {
        
            if (array_key_exists('AccountID', current($this->aRows))) {
                return current($this->aRows)['AccountID'];
            } else {
                return '';
            }

        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
