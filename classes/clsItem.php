<?php
    class clsItem
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lUnitID;

        function __construct() {

            $this->sQueryType = "select";
            $this->lUnitID = -1;

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                case "byunitid":

                    $sSql = "SELECT * FROM ProgressItems INNER JOIN Item ON ProgressItems.ItemKey = Item.Key WHERE ProgressUnitID = $this->lUnitID";

                    break;

                case "select":

                    $sSql = "SELECT * FROM Item";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function GetItemID () {

            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }

        }

        public function GetItemKey () {

            if ($this->sQueryType == 'select') {
                $sKey = 'Key';
            } else {
                $sKey = 'ItemKey';
            }

            if (array_key_exists($sKey, current($this->aRows))) {
                return current($this->aRows)[$sKey];
            } else {
                return '';
            }

        }

        public function GetItemName () {

            return clsDictionary::GetDicItem('Item_' . $this->GetItemKey());

        }

        public function GetItemMaxHP () {

            if (array_key_exists('MaxHP_Buff', current($this->aRows))) {
                return current($this->aRows)['MaxHP_Buff'];
            } else {
                return '';
            }

        }

        public function GetItemMaxAP () {

            if (array_key_exists('MaxAP_Buff', current($this->aRows))) {
                return current($this->aRows)['MaxAP_Buff'];
            } else {
                return '';
            }

        }

        public function GetItemPhysAttack () {

            if (array_key_exists('PhysicalAttack_Buff', current($this->aRows))) {
                return current($this->aRows)['PhysicalAttack_Buff'];
            } else {
                return '';
            }

        }

        public function GetItemMagicAttack () {

            if (array_key_exists('MagicAttack_Buff', current($this->aRows))) {
                return current($this->aRows)['MagicAttack_Buff'];
            } else {
                return '';
            }

        }

        public function GetItemDefense () {

            if (array_key_exists('Defense_Buff', current($this->aRows))) {
                return current($this->aRows)['Defense_Buff'];
            } else {
                return '';
            }

        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
