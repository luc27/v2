<?php
    class clsUnit
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lProfileID;
        public $lUnitID;
        public $lMatchID;

        function __construct() {

            $this->sQueryType = "select";
            $this->lProfileID = -1;
            $this->lUnitID = -1;
            $this->lMatchID = -1;

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                case "byprofileid":

                    $sSql = "SELECT * FROM ProgressUnits INNER JOIN Unit ON ProgressUnits.UnitKey = Unit.Key WHERE ProfileID = $this->lProfileID";

                    break;

                case "select":

                    $sSql = "SELECT * FROM Unit";

                    break;

                case "byunitid":

                    $sSql = "SELECT * FROM ProgressUnits INNER JOIN Unit ON ProgressUnits.UnitKey = Unit.Key WHERE ID = $this->lUnitID";

                    break;
                
                case "bymatchid":

                    $sSql = "SELECT * FROM ProgressUnits INNER JOIN Unit ON ProgressUnits.UnitKey = Unit.Key WHERE MatchID = $this->lMatchID";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }
        
        public function IsOwn () {

            return $this->GetUnitProfileID() == $_SESSION['lProfileID'];

        }
        
        public function GetUnitProfileID () {

            if (array_key_exists('ProfileID', current($this->aRows))) {
                return current($this->aRows)['ProfileID'];
            } else {
                return '';
            }

        }
        
        public function GetUnitAccountID () {

            if (array_key_exists('AccountID', current($this->aRows))) {
                return current($this->aRows)['AccountID'];
            } else {
                return '';
            }

        }
        
        public function GetUnitID () {

            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }

        }

        public function GetUnitStatusText () {

            if (array_key_exists('Status', current($this->aRows))) {
                if (current($this->aRows)['MatchID'] > -1) {
                    return "In Fight";
                } elseif ($this->GetCurrentHP() > 0) {
                    return "Ready";
                } else {
                    return "KO";
                }
            } else {
                return '';
            }

        }
        public function GetUnitStatus () {

            if (array_key_exists('Status', current($this->aRows))) {
                if (current($this->aRows)['MatchID'] > -1) {
                    return 1;
                } elseif ($this->GetCurrentHP() > 0) {
                    return 0;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }
        
        }

        public function SetUnitStatus ($lStatus) {

            $sSql = "UPDATE ProgressUnits SET Status = $lStatus WHERE ID = " . $this->GetUnitID();

            $oResult = DB::$oConn->query($sSql);

        }

        public function GetUnitKey () {

            if ($this->sQueryType == 'select') {
                $sKey = 'Key';
            } else {
                $sKey = 'UnitKey';
            }

            if (array_key_exists($sKey, current($this->aRows))) {
                return current($this->aRows)[$sKey];
            } else {
                return '';
            }

        }

        public function GetUnitName () {

            return clsDictionary::GetDicItem('Unit_' . $this->GetUnitKey());

        }

        public function GetUnitXP () {

            if (array_key_exists('XP', current($this->aRows))) {
                return current($this->aRows)['XP'];
            } else {
                return '';
            }

        }

        public function GetCurrentHP () {
            if (array_key_exists('CurrentHP', current($this->aRows))) {
                return current($this->aRows)['CurrentHP'];
            } else {
                return 0;
            }
        }

        public function GetCurrentAP () {
            if (array_key_exists('CurrentAP', current($this->aRows))) {
                $val = current($this->aRows)['CurrentAP'];
                if (is_null($val)) {
                    return $this->RefillAP();
                } else {
                    return $val;
                }
            } else {
                return 0;
            }
        }

        public function GetBaseHP () {
            if (array_key_exists('BaseHp', current($this->aRows))) {
                return current($this->aRows)['BaseHp'];
            } else {
                return 0;
            }
        }

        public function GetBaseAP () {
            if (array_key_exists('BaseAp', current($this->aRows))) {
                return current($this->aRows)['BaseAp'];
            } else {
                return 0;
            }
        }
        
        public function GetUnitHP () {
            if (array_key_exists('MaxHP', current($this->aRows))) {
                $val = current($this->aRows)['MaxHP'];
                if (is_null($val)) {
                    return $this->SetUnitHP();
                } else {
                    return (int)$val;
                }
            } else {
                return 0;
            }
        }
        
        public function GetUnitAP () {
            if (array_key_exists('MaxAP', current($this->aRows))) {
                $val = current($this->aRows)['MaxAP'];
                if (is_null($val)) {
                    return $this->SetUnitAP();
                } else {
                    return (int)$val;
                }
            } else {
                return 0;
            }
        }
        public function GetBasePhysicalAttack () {
            if (array_key_exists('BasePhysAttack', current($this->aRows))) {
                return current($this->aRows)['BasePhysAttack'];
            } else {
                return 0;
            }
        }

        public function GetBaseMagicAttack () {

            if (array_key_exists('BaseMagAttack', current($this->aRows))) {
                return current($this->aRows)['BaseMagAttack'];
            } else {
                return 0;
            }

        }

        public function GetBaseDefense () {

            if (array_key_exists('BaseDef', current($this->aRows))) {
                return current($this->aRows)['BaseDef'];
            } else {
                return 0;
            }

        }

        public function GetMatchID () {
            if (array_key_exists('MatchID', current($this->aRows))) {
                return current($this->aRows)['MatchID'];
            } else {
                return 0;
            }
        }

        public function GetUnitLevel () {

            for ($i=5; $i > 1; $i--) {
                if ($this->GetUnitXP() > clsBalancing::GetValue('Level' . $i . 'Req')) {
                    return $i;
                }
            }

            return 1;

        }
        
        public function GetHealingCosts () {
        
            return ceil($this->GetUnitGearScore() * clsBalancing::GetValue('unitHealCostsFactor') * (($this->GetUnitHP() - $this->GetCurrentHP()) / $this->GetUnitHP()));
        
        }
        
        public function GetRezzCosts () {
        
            return ceil($this->GetUnitGearScore() * clsBalancing::GetValue('unitRessurectionCostsFactor'));
        
        }
        
        public function Dismiss () {
            DB::$oConn->query("UPDATE ProgressItems SET ProgressUnitID = NULL WHERE ProgressUnitID = " . $this->GetUnitID());
            DB::$oConn->query("DELETE FROM ProgressUnits WHERE ID = " . $this->GetUnitID());
        }
        public function Flee () {
            $oResult = DB::$oConn->query("UPDATE ProgressUnits SET MatchID = NULL WHERE ID = " . $this->GetUnitID());
        }
        public function DoDmg ($lAmount) {
            $newval = max($this->GetUnitHP() - $lAmount, 0);
            $sSql = "UPDATE ProgressUnits SET CurrentHP = $newval WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
        }
        public function ApCost ($lAmount) {
            $newval = max($this->GetUnitAP() - $lAmount, 0);
            $sSql = "UPDATE ProgressUnits SET CurrentAP = $newval WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
        }
        public function AddAura ($Key, $RestTurns, $Caster) {
            $sSql = "INSERT INTO `ProgressAura` (ProgressUnitID, AuraKey, RestTurns, Caster) VALUES (" . $this->GetUnitID() . ", \"$Key\", $RestTurns, $Caster)";
            $oResult = DB::$oConn->query($sSql);
        }
        
        public function RefillAP () {

            $lMaxAP = $this->GetUnitAP();
            
            $sSql = "UPDATE ProgressUnits SET CurrentAP = $lMaxAP WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
            
            return $lMaxAP;

        }
        
        public function Heal () {

            $lMaxHP = $this->GetUnitHP();
            
            $sSql = "UPDATE ProgressUnits SET CurrentHP = MaxHP WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
         
        }
        
        public function SetItem () {
            
        }

        public function GetUnitGearScore () {

            return (
                $this->GetUnitHP() +
                $this->GetUnitAP() +
                ($this->GetUnitPhysAttackPower()) +
                ($this->GetUnitMagicAttackPower()) +
                ($this->GetUnitDefense() * 10)
            );

        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);
            $this->RecalculateStats();

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }
        
        /* stat calc */
        
        
        private function RecalculateStats () {
            $oResult = DB::$oConn->query("CALL UpdateStats(" . $this->GetUnitID() . ")");
            $this->Load();
        }

        public function GetUnitPhysAttackPower () {
            if (array_key_exists('PhysicalAttackPower', current($this->aRows))) {
                return current($this->aRows)['PhysicalAttackPower'];
            } else {
                return 0;
            }
            // $lUnitItemsPhysAttack = 0;
            // $lStatMultiplier = 1;

            // if ($this->GetUnitLevel() > 1) {
            //     $lStatMultiplier = clsBalancing::GetValue('Level' . $this->GetUnitLevel() . 'Multi');
            // }

            // $oItem = new clsItem();
            // $oItem->sQueryType = "byUnitID";
            // $oItem->lUnitID = $this->GetUnitID();
            // $oItem->Load();

            // if (!$oItem->Eof()) {

            //     while(!$oItem->Eof()) {

            //         $lUnitItemsPhysAttack += $oItem->GetItemPhysAttack();

            //         $oItem->MoveNext();

            //     }

            // }

            // return floor(($this->GetBasePhysicalAttack() + $lUnitItemsPhysAttack) * $lStatMultiplier);

        }

        public function GetUnitMagicAttackPower () {
            if (array_key_exists('MagicAttackPower', current($this->aRows))) {
                return current($this->aRows)['MagicAttackPower'];
            } else {
                return 0;
            }
            // $lUnitItemsMagicAttack = 0;
            // $lStatMultiplier = 1;

            // if ($this->GetUnitLevel() > 1) {
            //     $lStatMultiplier = clsBalancing::GetValue('Level' . $this->GetUnitLevel() . 'Multi');
            // }

            // $oItem = new clsItem();
            // $oItem->sQueryType = "byUnitID";
            // $oItem->lUnitID = $this->GetUnitID();
            // $oItem->Load();

            // if (!$oItem->Eof()) {

            //     while(!$oItem->Eof()) {

            //         $lUnitItemsMagicAttack += $oItem->GetItemMagicAttack();

            //         $oItem->MoveNext();

            //     }

            // }

            // return floor(($this->GetBaseMagicAttack() + $lUnitItemsMagicAttack) * $lStatMultiplier);

        }

        public function GetUnitDefense () {
            if (array_key_exists('Defense', current($this->aRows))) {
                return current($this->aRows)['Defense'];
            } else {
                return 0;
            }
            // $lUnitItemsDefense = 0;
            // $lStatMultiplier = 1;

            // if ($this->GetUnitLevel() > 1) {
            //     $lStatMultiplier = clsBalancing::GetValue('Level' . $this->GetUnitLevel() . 'Multi');
            // }

            // $oItem = new clsItem();
            // $oItem->sQueryType = "byUnitID";
            // $oItem->lUnitID = $this->GetUnitID();
            // $oItem->Load();

            // if (!$oItem->Eof()) {

            //     while(!$oItem->Eof()) {

            //         $lUnitItemsDefense += $oItem->GetItemDefense();

            //         $oItem->MoveNext();

            //     }

            // }

            // return floor(($this->GetBaseDefense() + $lUnitItemsDefense) * $lStatMultiplier);

        }

        public function SetUnitHP () {

            $lUnitItemsMaxHP = 0;
            $lStatMultiplier = 1;

            if ($this->GetUnitLevel() > 1) {
                $lStatMultiplier = clsBalancing::GetValue('Level' . $this->GetUnitLevel() . 'Multi');
            }

            $oItem = new clsItem();
            $oItem->sQueryType = "byUnitID";
            $oItem->lUnitID = $this->GetUnitID();
            $oItem->Load();

            if (!$oItem->Eof()) {

                while(!$oItem->Eof()) {

                    $lUnitItemsMaxHP += $oItem->GetItemMaxHP();

                    $oItem->MoveNext();

                }

            }
            
            $lMaxHP = floor(($this->GetBaseHP() + $lUnitItemsMaxHP) * $lStatMultiplier);
            
            $sSql = "UPDATE ProgressUnits SET MaxHP = $lMaxHP WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
            
            return $lMaxHP;

        }

        public function SetUnitAP () {

            $lUnitItemsMaxAP = 0;
            $lStatMultiplier = 1;

            if ($this->GetUnitLevel() > 1) {
                $lStatMultiplier = clsBalancing::GetValue('Level' . $this->GetUnitLevel() . 'Multi');
            }

            $oItem = new clsItem();
            $oItem->sQueryType = "byUnitID";
            $oItem->lUnitID = $this->GetUnitID();
            $oItem->Load();

            if (!$oItem->Eof()) {

                while(!$oItem->Eof()) {

                    $lUnitItemsMaxAP += $oItem->GetItemMaxAP();

                    $oItem->MoveNext();

                }

            }

            $lMaxAP = floor(($this->GetBaseAP() + $lUnitItemsMaxAP) * $lStatMultiplier);
            
            $sSql = "UPDATE ProgressUnits SET MaxAP = $lMaxAP WHERE ID = " . $this->GetUnitID();
            $oResult = DB::$oConn->query($sSql);
            
            return $lMaxAP;

        }

    }
?>
