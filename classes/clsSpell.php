<?php
    class clsSpell extends clsDbBase
    {
        public $sQueryType;
        public $lUnitID;
        public $lSpellID;
        public $sSpellKey;
        
        function __construct() {

            $this->sQueryType = "select";
            $this->lUnitID = -1;
            $this->lSpellID = -1;
            $this->sSpellKey = "";

        }

        public function Load () {
            
            switch (strtolower($this->sQueryType)) {

                case "byunitid":

                    $sSql = "SELECT * FROM `ProgressSpell` INNER JOIN Spell ON ProgressSpell.SpellKey = Spell.Key WHERE ProgressUnitID IS NULL OR ProgressUnitID = $this->lUnitID";

                    break;

                case "byid":

                    $sSql = "SELECT * FROM `ProgressSpell` WHERE `ID` = '$this->lSpellID'";

                    break;

                case "bykey":

                    $sSql = "SELECT * FROM `Spell` WHERE `Key` = '$this->sSpellKey'";

                    break;

                case "select":

                    $sSql = "SELECT * FROM `Spell`";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);
            
            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }
        
        public function GetID () {
            return $this->GetTableData('ID');
        }
        public function GetSpellKey () {
            return $this->GetTableData('SpellKey');
        }
        public function GetRange () {
            return $this->GetTableData('Range');
        }
        public function GetApCost () {
            return $this->GetTableData('ApCost');
        }
        public function GetCooldown () {
            return $this->GetTableData('Cooldown');
        }
        public function GetActivationType () {
            return $this->GetTableData('ActivationType');
        }
        public function IsUsableOnHostile () {
            return $this->GetTableData('UsableOnHostile') == 1;
        }
        public function IsUsableOnFriendly () {
            return $this->GetTableData('UsableOnFriendly') == 1;
        }
    }
?>
