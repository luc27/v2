<?php
    class clsProfile
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lAccountID;
        public $lProfileID;

        function __construct() {

            $this->sQueryType = "byAccountID";
            $this->lAccountID = -1;
            $this->lProfileID = -1;

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                case "byaccountid":

                    $sSql = "SELECT * FROM AccountProfiles WHERE AccountID = $this->lAccountID";

                    break;

                case "byprofileid":

                    $sSql = "SELECT * FROM AccountProfiles WHERE ID = $this->lProfileID";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function GetProfileID () {

            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }

        }

        public function GetProfileStatus () {

            if (array_key_exists('Status', current($this->aRows))) {
                return current($this->aRows)['Status'];
            } else {
                return '';
            }

        }

        public function GetMaxGold () {
            return $this->GetData("MaxGold");
        }
        public function GetMaxItems () {
            return $this->GetData("MaxItems");
        }
        public function GetMaxUnits () {
            return $this->GetData("MaxUnits");
        }
        
        public function GetData ($sField) {

            $sSql = "SELECT Value FROM `ProgressParams` WHERE `Key` = '$sField' AND ProfileID = " . $this->GetProfileID();
            $result = DB::$oConn->query($sSql);
            $value = mysqli_fetch_assoc($result);
            return $value['Value'];

        }
        
        public function GetItems () {

            $sSql = "SELECT count(*) as total FROM ProgressItems WHERE ProgressUnitID = NULL AND ProfileID = " . $this->GetProfileID();
            $result = DB::$oConn->query($sSql);
            $value = mysqli_fetch_assoc($result);
            return $value['total'];

        }
        
        public function GetUnits () {

            $sSql = "SELECT count(*) as total FROM ProgressUnits WHERE ProfileID = " . $this->GetProfileID();
            $result = DB::$oConn->query($sSql);
            $value = mysqli_fetch_assoc($result);
            return $value['total'];

        }
        
        public function GetProfileGold () {

            $lProfileGold = 0;

            $sSql = "SELECT * FROM ProgressParams WHERE `Key` = 'Gold' AND ProfileID = " . $this->GetProfileID();

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $lProfileGold = $aSqlRow['Value'];

                }

            }

            if ($lProfileGold == 0) {

                $sSql = "SELECT * FROM Parameter WHERE `Key` = 'Gold'";

                $oResult = DB::$oConn->query($sSql);

                if($oResult->num_rows > 0) {

                    while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                        $lProfileGold = $aSqlRow['Default'];

                    }

                }

            }

            return $lProfileGold;

        }

        public function SetProfileGold ($lNewProfileGold) {

            $lNewProfileGold = floor($lNewProfileGold);
            
            $sSql = "SELECT * FROM ProgressParams WHERE `Key` = 'Gold' AND ProfileID = " . $this->GetProfileID();

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                $sSql = "UPDATE ProgressParams SET Value = '$lNewProfileGold' WHERE `Key` = 'Gold' AND ProfileID = " . $this->GetProfileID();

            } else {

                $sSql = "INSERT INTO ProgressParams (ProfileID,Key,Value) VALUES('" . $this->GetProfileID() . "', 'Gold', '$lNewProfileGold')";

            }

            $oResult = DB::$oConn->query($sSql);

        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
