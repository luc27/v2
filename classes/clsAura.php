<?php
    class clsAura
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lUnitID;
        public $lAuraID;
        
        function __construct() {

            $this->sQueryType = "select";
            $this->lUnitID = -1;
            $this->lAuraID = -1;

        }

        public function Load () {
            
            switch (strtolower($this->sQueryType)) {

                case "byunitid":

                    $sSql = "SELECT * FROM ProgressAura INNER JOIN Aura ON ProgressAura.AuraKey = Aura.Key WHERE ProgressUnitID = $this->lUnitID";

                    break;

                case "byid":

                    $sSql = "SELECT * FROM `ProgressAura` WHERE `ID` = '$this->lAuraID'";

                    break;

                case "select":

                    $sSql = "SELECT * FROM `ProgressAura`";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);
            
            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function GetID () {
            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }
        }
        
        public function GetCaster () {
            if (array_key_exists('Caster', current($this->aRows))) {
                return current($this->aRows)['Caster'];
            } else {
                return '';
            }
        }
        
        public function GetKey () {
            if (array_key_exists('AuraKey', current($this->aRows))) {
                return current($this->aRows)['AuraKey'];
            } else {
                return 0;
            }
        }
        
        public function GetRestTurns () {
            if (array_key_exists('RestTurns', current($this->aRows))) {
                return current($this->aRows)['RestTurns'];
            } else {
                return 0;
            }
        }
        
        public function GetHP () {
            if (array_key_exists('HP', current($this->aRows))) {
                return current($this->aRows)['HP'];
            } else {
                return 0;
            }
        }
        
        private function SetAndReload ($innerQuery) {
            $AID = $this->GetID();
            $sSql = "UPDATE `ProgressAura`set $innerQuery Where ID = $AID";
            $oResult = DB::$oConn->query($sSql);
            $this->Load();
        }
        
        private function Delete () {
            $AID = $this->GetID();
            $sSql = "DELETE FROM `ProgressAura` Where ID = $AID";
            $oResult = DB::$oConn->query($sSql);
            $this->Load();
        }
        
        private function Burn () {
            if ($this->GetRestTurns() > 1) {
                $this->SetAndReload("RestTurns = RestTurns - 1");
            } else {
                $this->Delete();
            }
        }
        
        public function Process ($target) {
            $oUnit = new clsUnit();
            $oUnit->sQueryType = "byUnitID";
            $oUnit->lUnitID = $this->GetCaster();
            $oUnit->Load();
            
            $target->DoDmg($this->GetHP());
            
            $this->Burn();
        }
        
        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
