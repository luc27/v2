<?php
    class clsMatch
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lMatchID;
        public $lAccountID;
        
        function __construct() {

            $this->sQueryType = "select";
            $this->lMatchID = -1;
            $this->lAccountID = -1;

        }

        public function Load () {
            
            switch (strtolower($this->sQueryType)) {

                case "byaccountid":

                    $sSql = "SELECT * FROM `Match` WHERE AccountID = $this->lAccountID or Player2 = $this->lAccountID";

                    break;

                case "byid":

                    $sSql = "SELECT * FROM `Match` WHERE `ID` = '$this->lMatchID'";

                    break;

                case "select":

                    $sSql = "SELECT * FROM `Match`";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);
            
            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function GetID () {
        
            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }

        }

        public function GetCurrentTeam () {
        
            if (array_key_exists('CurrentTeam', current($this->aRows))) {
                return current($this->aRows)['CurrentTeam'];
            } else {
                return '';
            }

        }

        public function GetAccountID2 () {
        
            if (array_key_exists('Player2', current($this->aRows))) {
                return current($this->aRows)['Player2'];
            } else {
                return '';
            }

        }

        public function GetStatus () {
        
            if (array_key_exists('Status', current($this->aRows))) {
                return current($this->aRows)['Status'];
            } else {
                return '';
            }

        }
        
        public function IsSetup () {
            return $this->GetStatus() <= 1;
        }
        public function IsOngoing () {
            return $this->GetStatus() == 2;
        }
        public function IsOver () {
            return $this->GetStatus() == 3;
        }
        public function IsWon () {
            return $this->IsOver() && $this->IsOwnTurn();
        }
        public function GetStatusText () {
           if ($this->IsSetup()) return "Setup phase";
           if ($this->IsOngoing()) {
             return $this->IsOwnTurn() ? "Your turn" : "Enemy's turn";
            } 
            if ($this->IsOver()) {
             return $this->IsOwnTurn() ? "Won" : "Lost";
            }
        }

        public function GetAccountID () {
        
            if (array_key_exists('AccountID', current($this->aRows))) {
                return current($this->aRows)['AccountID'];
            } else {
                return '';
            }

        }

        public function GetOwnPot () {
            if ($this->IsAttacking()) {
                return $this->GetPot1();
            } else {
                return $this->GetPot2();
            }
        }
        public function GetPot2 () {
            if (array_key_exists('Pot2', current($this->aRows))) {
                return current($this->aRows)['Pot2'];
            } else {
                return '';
            }
        }
        public function GetPot1 () {
            if (array_key_exists('Pot1', current($this->aRows))) {
                return current($this->aRows)['Pot1'];
            } else {
                return '';
            }
        }
        
        private function SetAndReload ($innerQuery) {
            $sSql = "UPDATE `Match`set $innerQuery Where ID = " . $this->GetID();
            $oResult = DB::$oConn->query($sSql);
            $this->Load();
        }
        
        public function FreeAllUnits () {
            $sSql = "UPDATE ProgressUnits SET MatchID = NULL WHERE MatchID = " . $this->GetID();
            $oResult = DB::$oConn->query($sSql);
        }
        public function Delete () {
            $sSql = "DELETE FROM `Match` WHERE `ID` = " . $this->GetID();
            $oResult = DB::$oConn->query($sSql);
        }
        
        private function ProcessAuras () {
            $oUnit = new clsUnit();
            $oUnit->sQueryType = "byMatchID";
            $oUnit->lMatchID = $this->GetID();
            $oUnit->Load();
            
            $oAura = new clsAura();
            $oAura->sQueryType = "byUnitID";
            
            while(!$oUnit->Eof()) {
            
                $oAura->lUnitID = $oUnit->GetUnitID();
                $oAura->Load();
                
                while(!$oAura->Eof()) {
                    
                    $oAura->Process($oUnit);
                    $oAura->MoveNext();
                }
                
                $oUnit->MoveNext();
            } 
        }
        
        public function EndTurn () {
            
            // $this->ProcessAuras();
            
            $AI1 = $this->GetAccountID();
            $AI2 = $this->GetAccountID2();
            
            // check if team 1 is down
            if (!DB::RowExists("SELECT 1 FROM `ProgressUnits` WHERE MatchID = $this->lMatchID AND `AccountID` = $AI1")){
            
                $this->SetAndReload("CurrentTeam = 1, Status = 3");
                $this->FreeAllUnits();
                
                // check if team 2 is down
            } else if (!DB::RowExists("SELECT 1 FROM `ProgressUnits` WHERE MatchID = $this->lMatchID AND `AccountID` = $AI2")){
                
                $this->SetAndReload("CurrentTeam = 2, Status = 3");
                $this->FreeAllUnits();
            
            // refresh AP
            } else {
                $this->SetAndReload("CurrentTeam = (CurrentTeam % 2) + 1");
                
                $sSql = "UPDATE ProgressUnits SET CurrentAP = MaxAP WHERE MatchID = $this->lMatchID";
                $oResult = DB::$oConn->query($sSql);
            }
            
        }
        public function GetCurrentAccountID () {
            if ($this->GetCurrentTeam() == 2) {
                return $this->GetAccountID2();
            } else {
                return $this->GetAccountID();
            }
        }
        public function GetEnemyAccountID () {
            if ($this->IsAttacking()) {
                return $this->GetAccountID2();
            } else {
                return $this->GetAccountID();
            }
        }
        public function IsAttacking () {
            return $this->GetAccountID() == $_SESSION['lAccountID'];
        }
        public function IsDefending () {
            return $this->GetAccountID2() == $_SESSION['lAccountID'];
        }
        public function IsOwnTurn () {
            return ($this->GetCurrentTeam() == 1 && $this->IsAttacking()) || ($this->GetCurrentTeam() == 2 && $this->IsDefending());
        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
