<?php
    function MyIf ($bCondition, $sString1, $sString2) {

        if ($bCondition) {

            return $sString1;

        } else {

            return $sString2;

        }

    }

    function MyTrim ($sString1, $sString2) {

        return MyIf($sString1 != '', $sString1, $sString2);

    }

    function GetSvgIcon ($sSvgName) {

        if (file_exists("default/images/svg/" . $sSvgName . ".svg")) {
            echo file_get_contents("default/images/svg/" . $sSvgName . ".svg");
        }

    }

    function GenerateRandomPassword ($lLength = 8)
    {
      $sGeneratedPassword = "";
      $sPossibleChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      $i = 0;
      while ($i < $lLength) {
        $sChar = substr($sPossibleChars, mt_rand(0, strlen($sPossibleChars)-1), 1);
        if (!strstr($sGeneratedPassword, $sChar)) {
          $sGeneratedPassword .= $sChar;
          $i++;
        }
      }
      return $sGeneratedPassword;
    }
?>
