<?php
    class clsDbBase
    {

        protected $i = 0;
        protected $aRows;
        
        protected function GetTableData ($column) {
            if (array_key_exists($column, current($this->aRows))) {
                return current($this->aRows)[$column];
            } else {
                return '';
            }
        }
        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
