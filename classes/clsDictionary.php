<?php
    class clsDictionary
    {
        private $i;

        public static $sLanguageCode = 'EN';

        public static function GetDicItem ($sDicKey) {

            $sSql = "SELECT " . self::$sLanguageCode . "Text FROM Dictionary WHERE `Key` = '$sDicKey'";

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

            	while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    if (array_key_exists(self::$sLanguageCode . 'Text', $aSqlRow)) {
                        return $aSqlRow[self::$sLanguageCode . 'Text'];
                    } else {
                        return '';
                    }

            	}
            }
        }
    }
?>
