<?php
    class clsAccount
    {

        private $i = 0;
        private $aRows;

        public $sQueryType;
        public $lAccountID;

        function __construct() {

            $this->sQueryType = "byAccountID";
            $this->lAccountID = -1;

        }

        public function Load () {

            switch (strtolower($this->sQueryType)) {

                case "byaccountid":

                    $sSql = "SELECT * FROM Account WHERE ID = $this->lAccountID";

                    break;

                case "select":

                    $sSql = "SELECT * FROM Account";

                    break;

                default:

                    echo "No valid QueryType: $this->sQueryType!";
                    die();

                    break;

            }

            $oResult = DB::$oConn->query($sSql);

            if($oResult->num_rows > 0) {

                while($aSqlRow = mysqli_fetch_assoc($oResult)) {

                    $this->aRows[$this->i] = $aSqlRow;

                    $this->i = $this->i + 1;

                }

                $this->i = 0;

            }

        }

        public function IsAI () {

            if (array_key_exists('IsAI', current($this->aRows))) {
                return current($this->aRows)['IsAI'];
            } else {
                return '';
            }

        }
        public function GetAccountID () {

            if (array_key_exists('ID', current($this->aRows))) {
                return current($this->aRows)['ID'];
            } else {
                return '';
            }

        }

        public function GetAccountName () {

            if (array_key_exists('Username', current($this->aRows))) {
                return current($this->aRows)['Username'];
            } else {
                return '';
            }

        }

        public function Eof () {

            if (!empty($this->aRows)) {

                return MyIf(current($this->aRows) == "", true, false);

            } else {

                return true;

            }

        }

        public function MoveFirst () {

            reset($this->aRows);

        }

        public function MoveNext () {

            next($this->aRows);

        }

        public function MovePrev () {

            prev($this->aRows);

        }

        public function MoveLast () {

            end($this->aRows);

        }

    }
?>
