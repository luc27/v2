<?php
    function ResetAccount ($lAccountID) {

        $oProfile = new clsProfile();
        $oProfile->lAccountID = $lAccountID;
        $oProfile->Load();

        if (!$oProfile->Eof()) {

            while(!$oProfile->Eof()) {

                $lProfileID = $oProfile->GetProfileID();

                $sSql = "DELETE FROM ProgressItems WHERE ProfileID = $lProfileID";

                $oResult = DB::$oConn->query($sSql);

                $sSql = "DELETE FROM ProgressUnits WHERE ProfileID = $lProfileID";

                $oResult = DB::$oConn->query($sSql);

                $sSql = "DELETE FROM ProgressParams WHERE ProfileID = $lProfileID";

                $oResult = DB::$oConn->query($sSql);

                $sSql = "DELETE FROM ProgressBuildings WHERE ProfileID = $lProfileID";

                $oResult = DB::$oConn->query($sSql);

                $sSql = "DELETE FROM AccountProfiles WHERE ID = $lProfileID";

                $oResult = DB::$oConn->query($sSql);

                $oProfile->MoveNext();
            }
        }

        $sSql = "INSERT INTO AccountProfiles (AccountID, Status) VALUES ($lAccountID, '1')";

        $aResult = mysqli_query(DB::$oConn, $sSql);

        $lProfileID = DB::$oConn->insert_id;

        $sSql = "INSERT INTO ProgressUnits (ProfileID, UnitKey, XP, Status) VALUES ($lProfileID, 'Recruit', '0', 1)";

        $aResult = mysqli_query(DB::$oConn, $sSql);

        $lProgressUnitID = DB::$oConn->insert_id;

        $sSql = "INSERT INTO ProgressItems (ProfileID, ProgressUnitID, ItemKey) VALUES ($lProfileID, $lProgressUnitID, 'HPPotion1')";

        $aResult = mysqli_query(DB::$oConn, $sSql);
    }

    function deleteProfile ($lProfileID) {

        $sSql = "DELETE FROM ProgressItems WHERE ProfileID = $lProfileID";

        $oResult = DB::$oConn->query($sSql);

        $sSql = "DELETE FROM ProgressUnits WHERE ProfileID = $lProfileID";

        $oResult = DB::$oConn->query($sSql);

        $sSql = "DELETE FROM ProgressParams WHERE ProfileID = $lProfileID";

        $oResult = DB::$oConn->query($sSql);

        $sSql = "DELETE FROM ProgressBuildings WHERE ProfileID = $lProfileID";

        $oResult = DB::$oConn->query($sSql);

        $sSql = "DELETE FROM AccountProfiles WHERE ID = $lProfileID";

        $oResult = DB::$oConn->query($sSql);
    }
?>
