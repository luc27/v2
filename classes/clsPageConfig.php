<?php
    class clsPageConfig
    {

        public $sFirstMd5Salt;
        public $sSecondMd5Salt;

        public $sLanguageCode;

        function __construct() {

            $this->sFirstMd5Salt = 'gamskopf';
            $this->sSecondMd5Salt = 'marende und 5e vom schwemm';

            $this->sLanguageCode = 'EN';

        }

        public function GetMd5 ($sString) {

            $sString = md5($sString);
            $sString = md5($this->sFirstMd5Salt . $sString . $this->sSecondMd5Salt);

            return $sString;

        }

        public function SecureThisPage($sRedirectUrl) {

            if (!isset($_SESSION['lAccountID'])) {
                header('Location: ' . $sRedirectUrl);
            }
            
            if (!isset($_SESSION['lProfileID'])) {
                header('Location: ' . $sRedirectUrl);
            }

        }
        
        public function To($sRedirectUrl) {
        
          header('Location: ' . $sRedirectUrl);
        
        }

    }
?>
